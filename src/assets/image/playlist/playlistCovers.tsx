import { PlaylistCover } from "../../../utils/types/type"

export const playlistCovers: PlaylistCover[] = [
    {
        playlistId: 1,
        cover: require('./breathe.jpg')
    },
    {
        playlistId: 2,
        cover: require('./lofiVibes.jpg')
    },
    {
        playlistId: 3,
        cover: require('./likedSongs.jpg')
    }
]