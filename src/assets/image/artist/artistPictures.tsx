import { ArtistPicture } from '../../../utils/types/type'

export const artistPictures: ArtistPicture[] = [
    {
        artistId: 1,
        picture: require('./anbuu.jpg')
    },
    {
        artistId: 2,
        picture: require('./blue_wednesday.jpg')
    },
    {
        artistId: 3,
        picture: require('./yasumu.jpg')
    },
    {
        artistId: 4,
        picture: require('./swink.jpg')
    },
    {
        artistId: 5,
        picture: require('./pandrezz.jpg')
    },
    {
        artistId: 6,
        picture: require('./nymano.jpg')
    },
    {
        artistId: 7,
        picture: require('./idealism.jpg')
    },
    {
        artistId: 8,
        picture: require('./nothingtosay.jpg')
    },
    {
        artistId: 9,
        picture: require('./saib.jpg')
    },
    {
        artistId: 10,
        picture: require('./kupla.jpg')
    },

    
]