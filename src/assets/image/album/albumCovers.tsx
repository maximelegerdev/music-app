import {AlbumCover} from '../../../utils/types/type'; 

export const albumCovers: AlbumCover[] = [
    {
        albumId: 1,
        cover: require('./hibernation.jpg')
    }
]