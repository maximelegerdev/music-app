//Imports nécessaires au fonctionnement du composant
import React, { ReactNode, useEffect } from 'react';
import styled from 'styled-components';
import songs from '../data/songs'
import {useState, useContext} from 'react';
import {useRef} from 'react';
import '../styles/musicPlayer.scss';
import '../styles/range.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faShuffle, faRepeat,faPlay, faPause, faBackward, faForward, faVolumeUp, faVolumeDown, faVolumeMute} from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { sortSongsById, timeFormat, getAudioAndArtistNamesForSong } from '../utils/functions/globalFunctions';
import { SongSourceContext } from '../utils/context/SongSourceContext';
import {Album, Playlist, Song} from '../utils/types/type'
import { checkPlaylist} from '../utils/types/typeCheckingConstants';
import { SongContext } from '../utils/context/SongContext';

//CSS du conteneur du Music Player
const MusicContainer = styled.div`
    background-color: #23263d;
    color: #ffffff;
    display:flex;
    align-items: center;    
    text-align: center;
    flex-direction: column;
    min-width: 325px;
    padding : 0px 15px 0px;
    font-size: 100;
    z-index: 1;
    box-shadow: -6px 0px 10px 2px rgba(0, 0, 0, 0.3);
`

//CSS de l'image
const ImageContainer = styled.div`
    position: relative;
    margin-top: 20px;
    width : 200px;
`

const StyledImage = styled.img`
    width : inherit;
    height : 200px;
    border-radius : 6%;
    object-fit : cover;
    box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.4)
`

//CSS des infos
const MusicInfoWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
    jusitfy-content: center;
    padding : 5px;
    align-content: space-around;
`

const Names = styled.div`
    line-height: 8px;
    margin : 8px; 
`
//CSS des infos, plus spécifiquement de la barre de progression de la musique (suite dans musicPlayer.css)
const ProgressInfoWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding: 8px 8px;
    border-radius: 6px;
    box-shadow: 5px 5px 7px rgba(0, 0, 0, 0.4);
    width: 95%;
    align-items: center;
`

const ProgressWrapper = styled.div`
    display: flex;
    flex-direction: row;
    width: 70%;
    justify-content: center;
`

//CSS de la navigation
const MusicNav = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding : 10px 0;
`

const MusicControls = styled.div`
    display: flex;
    flex-direction: row;
    align-content: center;
    justify-content: center;
    box-shadow: 5px 5px 6px rgba(0, 0, 0, 0.4);
    margin: 5px;
    border-radius: 10px;
    padding: 10px 5px;
    width: 100%;
`

const VolumeContainer = styled.label`
    display: flex;
    flex-direction: column;
    padding: 8px;
`

//Typage des données
export type MusicInfoProps = {
    title: string,
    artist: string,
    actualTime: number,
    totalTime: number,
}

//Composant Music Player
function MusicPlayer() {

    //Refs
    const audioRef = useRef(document.createElement("audio") );
    const currentTimeRef = useRef(document.createElement("div"));
    const progressBarRef = useRef(document.createElement("div"));
    const progressContainerRef = useRef(document.createElement("div"));
    const volumeRef = useRef(document.createElement("input"));
    const volumeFillRef = useRef(document.createElement("span"));
    const shuffleRef = useRef(document.createElement("div"));
    const repeatRef = useRef(document.createElement("div"));

    //States
    const [playingSongId, setPlayingSongId] = useState(0);
    const [isPlaying, setIsPlaying] = useState(false);
    const [isFirstPlay, setIsFirstPlay] = useState(true);
    const [isShuffle, setIsShuffle] = useState(false);
    const [playPause, setPlayPause] = useState(faPlay);
    const [volumeIcon, setVolumeIcon] = useState(faVolumeUp);
    const [isRepeat, setIsRepeat] = useState(false); 
    const [activePlayPause, setActivePlayPause] = useState("");

    //Contextes
    const {songSource, setSongSource} = useContext(SongSourceContext);
    const {contextSong, setContextSong, songState, setSongState} = useContext(SongContext);
    const [songList, setSongList] = useState<Song[]>([])

    progressContainerRef.current.addEventListener('click', setProgress);

    //Permet d'ajouter l'attribut autoPlay à l'audio
    function activateAutoPlay() {
        if(isFirstPlay) {
            audioRef.current.setAttribute('autoPlay', '');
            setIsFirstPlay(false);
        }
    }

    //Permet de jouer l'audio
    function playSong(){
        setActivePlayPause("play-pause-active");
        audioRef.current.play();
        setSongState(true);
        setPlayPause(faPause);
        activateAutoPlay();
    }

    //Permet de pauser l'audio
    function pauseSong(){
        setActivePlayPause("");
        audioRef.current.pause();
        setSongState(false);
        setPlayPause(faPlay);
    }

    //Permet de revenir à la chanson précédente, ou de replay la chanson actuelle si le replay est enclenché 
    function prevSong() {
        if(audioRef.current.currentTime > 2 || isRepeat === true) {
            audioRef.current.currentTime = 0;
        }  
        else if(isRepeat === false) {
            (playingSongId - 1 < 0) ? setPlayingSongId(songList.length-1) : setPlayingSongId(playingSongId-1);
            setContextSong(songList[playingSongId]);    
        } 
        activateAutoPlay();
        setActivePlayPause("play-pause-active");
        setSongState(true);
        setPlayPause(faPause);
    }

    //Permet de passer à la chanson suivante, ou de replay la chanson actuelle si le replay est enclenché
    function nextSong() {
        
        if (isRepeat === false) {
            if(isShuffle === false){
                (playingSongId + 1 > songList.length - 1) ? setPlayingSongId(0) : setPlayingSongId(playingSongId+1);
            } else {
                let id: number;
                do{
                    id= Math.floor(Math.random() * songList.length);
                } while(id === playingSongId);
                setPlayingSongId(id);
            }
            setContextSong(songList[playingSongId]);
        } else {
            audioRef.current.currentTime = 0;
        }
        activateAutoPlay();
        setActivePlayPause("play-pause-active");
        setSongState(true);
        setPlayPause(faPause);
    }

    //Permet d'intervertir les icônes faPause et faPlay et d'appeler playSong() ou pauseSong() en fonction du contexte
    function songControl() {
        if(songState){
            pauseSong();
        } else {
            playSong();
        };
    }

    //Permet de faire avancer la barre de progression et le temps actuel du morceau automatiquement
    function progress() {
        //Avancée de la progressBar
        const cur: number = audioRef.current.currentTime;
        const dur: number = audioRef.current.duration;
        const progressPercent: number = (cur/dur)*100;
        progressBarRef.current.style.width = progressPercent.toString() + '%';

        //Avancée du temps
        currentTimeRef.current.innerHTML = timeFormat(audioRef.current.currentTime);
    }

    // Permet de faire avancer le morceau, la barre de progression et la visualisation du temps actuel de la chanson en fonction
    // de la position du clic de l'utilisateur sur la barre de progression
    function setProgress(e: MouseEvent) {
        const width = progressContainerRef.current.clientWidth;
        const clickX = e.offsetX;
        const duration = audioRef.current.duration;

        audioRef.current.currentTime = (clickX/width) * duration;
    }

    //Permet de changer le volume en fonction du clic de l'utilisateur sur la barre de volume
    function changeVolume() {
        
        audioRef.current.volume = parseInt(volumeRef.current.value)/100;
        const volumeRange: number = audioRef.current.volume;
        if(volumeRange < 0.5 && volumeRange !== 0) {
            setVolumeIcon(faVolumeDown);
        } else if (volumeRange === 0) {
            setVolumeIcon(faVolumeMute);
        } else {
            setVolumeIcon(faVolumeUp);
        }
        volumeFillRef.current.style.width = volumeRef.current.value.toString() + '%';
    }

    //Permet de passer en mute le morceau
    function muteSong(){
        setVolumeIcon(faVolumeMute); 
        volumeFillRef.current.style.width = "0%"; 
        volumeRef.current.value = "0"
    }

    //Permet d'activer ou désactiver le shuffle, tout en changeant le CSS en rapport avec cette activiation/désactivation
    function controlShuffle(){
       if(isShuffle === true) {
            shuffleRef.current.className="shuffle-repeat";
            setIsShuffle(false);
       } else {
            shuffleRef.current.className="shuffle-repeat active";
            setIsShuffle(true);
       }
    }

    //Permet d'activer ou désactiver le repeat, tout en changeant le CSS en rapport avec cette activiation/désactivation
    function controlRepeat(){
        if(isRepeat === true) {
            repeatRef.current.className = "shuffle-repeat";
            setIsRepeat(false);
        } else {
            repeatRef.current.className = "shuffle-repeat active";
            setIsRepeat(true);
        }
    }

    //Permet de retourner le contenu de la div de la songSource 
    function songSourceContent(songSource : Playlist | Album){
        if ('playlistName' in songSource){
            if(songSource.playlistName === "Catalog" || songSource.playlistName === "Search results" 
                || songSource.playlistName === "Recently Played"){
                return(
                    <>
                        <p>PLAYING FROM</p>
                        <a>{songSource.playlistName.toUpperCase()}</a>
                    </>
                )
            }
            return (
                <>
                    <p>PLAYING FROM PLAYLIST</p>
                    <a>{songSource.playlistName.toUpperCase()}</a>
                </>
            )
        } else if ('albumTitle' in songSource){
            return (
                <>
                    <p>PLAYING FROM ALBUM</p>
                    <a>{songSource.albumTitle.toUpperCase()}</a>
                </>
            )
        }
    }

    getAudioAndArtistNamesForSong(contextSong!);

    //Définition du useEffect
    useEffect(() => {
        async function fetchSongsFromSource(songSource: Playlist | Album) {
            if(typeof(songSource) === typeof(checkPlaylist) && 'songsIds' in songSource){
                let tableSongs : Song[] = [];
                const songsIds = (songSource.playlistId === -1) ? JSON.parse(songSource.songsIds).recentlyPlayedIds : JSON.parse(songSource.songsIds).songsIds;
                if (songSource.playlistId === -1) {
                    songsIds.reverse();
                }
                songsIds.forEach(async (songId: number) => {
                        await axios.get("http://localhost:8080/song/"+songId)
                        .then((response) => {
                            tableSongs.push(response.data);
                            if(tableSongs.length === songsIds.length) {
                                setSongList(sortSongsById(tableSongs)); 
                            }  
                        })
                        .catch(() => "Couldn't fetch songs from playlist")
                    }
                )
            } else if('albumId' in songSource) {
                let tableSongs : Song[] = [];
                await axios.get("http://localhost:8080/album/albumSongs/"+ songSource.albumId)
                .then((response) => {
                    setSongList(response.data);
                })
                .catch(() => "Couldn't fetch songs from album")
            }
        }
        fetchSongsFromSource(songSource);
        songState ? playSong() : pauseSong();
        
    }, [songSource, songState])


    //Retour du composant
    return(
        <MusicContainer>
            <div className='songSource'>
                {songSourceContent(songSource)}
            </div>
            <h1 className='now-playing'>Now Playing</h1>
            <ImageContainer>
                {/* <StyledImage src={songs[songId].cover} alt={songs[songId].alt} title={songs[songId].alt} ></StyledImage> */}
                <StyledImage src={contextSong!.cover} alt={contextSong!.coverAlt} title={contextSong!.coverAlt} ></StyledImage>
            </ImageContainer>
            {/* <audio ref={audioRef} src={songs[songId].audio} onTimeUpdate={progress} onEnded={nextSong}></audio> */}
            <audio ref={audioRef} src={getAudioAndArtistNamesForSong(contextSong!).audio} onTimeUpdate={progress} onEnded={nextSong}></audio>
            <MusicInfoWrapper>
                <Names>
                    <h3 className="song-title-music-player">{contextSong!.songTitle}</h3>
                    {/* <h3 className="song-title-music-player">{songs[songId].songTitle}</h3> */}
                    {/* <p className="song-artist-music-player">{songs[songId].artist}</p> */}
                    <p className="song-artist-music-player">{getAudioAndArtistNamesForSong(contextSong!).artist}</p>
                </Names>
                <ProgressInfoWrapper>
                    <div className="song-time" ref={currentTimeRef}>0:00</div>
                    <ProgressWrapper>
                        <div className="progress-container" ref={progressContainerRef} onClick={() => setProgress}>
                            <div ref={progressBarRef} className="progress"></div>
                        </div>
                    </ProgressWrapper>
                    <div className="song-time">{songs[contextSong!.songId - 1].duration}</div>
                </ProgressInfoWrapper>
            </MusicInfoWrapper>

            <MusicNav>

                <MusicControls>
                    <div ref={shuffleRef} className="shuffle-repeat" onClick={() => controlShuffle()}>
                        <FontAwesomeIcon icon={faShuffle} />
                    </div>
                    <div className="main-controls">
                        <FontAwesomeIcon onClick={prevSong} className="forward-backward" icon={faBackward} />
                        <FontAwesomeIcon onClick={songControl} className={"play-pause " + activePlayPause} icon={playPause} />
                        <FontAwesomeIcon onClick={nextSong} className="forward-backward" icon={faForward} />
                    </div>
                    <div ref={repeatRef} className="shuffle-repeat" onClick={() => controlRepeat()}>
                        <FontAwesomeIcon icon={faRepeat} />
                        <p className="repeat-song-mode"></p>
                    </div>
                </MusicControls>
                <div className="volume">
                    <FontAwesomeIcon icon={volumeIcon} className="volume-icon" onClick = {() => muteSong()} />
                    <div className="rangeContainer">
                        <span className="bar"><span className="fill" ref={volumeFillRef}></span></span>
                        <input type="range" className="volumeSlider" ref={volumeRef} onClick={changeVolume} min="0" max="100" />
                    </div>
                </div>
            </MusicNav>
        </MusicContainer>
    );
}

export default MusicPlayer

