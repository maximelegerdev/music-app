import React from 'react';
import styled from 'styled-components';
import '../styles/navbar.scss';
import {useState} from 'react';
import {Link} from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faHouse, faCompactDisc, faList, faMagnifyingGlass} from '@fortawesome/free-solid-svg-icons';
import logo from '../assets/image/logo-mock.png'

const NavbarWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-width: 80px;
    background-color : #23263d;
    color: white;
    box-shadow : 5px 0px 10px 2px rgba(0, 0, 0, 0.4);
    z-index: 1;
`

const LogoImg = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 40px;
`
const IconContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100%;
    margin-bottom: 50px;
`


function Navbar () {


    //Etablissement des states
    const [activeHome, setActiveHome] = useState("icon active");
    const [activeSearch, setActiveSearch] = useState("icon");
    const [activeLibrary, setActiveLibrary] = useState("icon");
    const [activeWaitingList, setActiveWaitingList] = useState("icon");

    //Etablissement des fonctions

    //Fonction permettant d'appliquer la classe active sur l'icône de la page active
    function applyCssChanges(page: string) {
        console.log(page);
        switch(page) {
            case "home" : 
                setActiveHome("icon active");
                setActiveSearch("icon");
                setActiveLibrary("icon");
                setActiveWaitingList("icon");
                break;
            case "search" : 
                setActiveHome("icon");
                setActiveSearch("icon active");
                setActiveLibrary("icon");
                setActiveWaitingList("icon");
                break;
            case "library" : 
                setActiveHome("icon");
                setActiveSearch("icon");
                setActiveLibrary("icon active");
                setActiveWaitingList("icon");
                break;
            case "waitingList" : 
                setActiveHome("icon");
                setActiveSearch("icon");
                setActiveLibrary("icon");
                setActiveWaitingList("icon active");
                break;
        }
    }


    //Retour du composant permettant de faire fonctionner le router
    return( 
        <NavbarWrapper> 
            <LogoImg>
                <img alt={"Logo de Mocktify"} src={logo} className="logo" title="Logo de Mocktify"></img>
            </LogoImg> 
            <IconContainer>

                <Link className='link' to="/" >
                    <div className={activeHome} onClick={() => {applyCssChanges("home")}}>
                        <FontAwesomeIcon icon={faHouse}/>
                    </div>
                </Link>
                <Link className='link' to="/Search">
                    <div className={"icon " + activeSearch} onClick={() => {applyCssChanges("search")}}>
                        <FontAwesomeIcon icon={faMagnifyingGlass}/>
                    </div>
                </Link> 
                <Link  className='link' to="/Library">
                    <div className={"icon " + activeLibrary} onClick={() => {applyCssChanges("library")}}>
                        <FontAwesomeIcon  icon={faCompactDisc}/>
                    </div>
                </Link>
                <Link className='link' to="/WaitingList">
                    <div  className={"icon " + activeWaitingList} onClick={() => {applyCssChanges("waitingList")}}>
                        <FontAwesomeIcon  icon={faList}/>
                    </div>
                </Link>

            </IconContainer>
        </NavbarWrapper>
    );
}


export default Navbar;