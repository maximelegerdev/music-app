import React, { useState, useRef } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import artists from '../../data/artists'
import '../../styles/library.scss'
import { Artist } from '../../utils/types/type'

//Définition des styled-components
const LikedArtistCard = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    height: 170px;
    min-width: 140px;
    border-radius: 10px;
    margin: 0 15px 0 0px;
    background-color: #23263d;
    box-shadow: 5px 5px 10px 2px rgba(0, 0, 0, 0.4);
    cursor: pointer;
    transition: all 0.1s ease-out;

    &:hover{
        box-shadow: 6px 6px 12px 2px rgba(0, 0, 0, 0.4);
        transform: translate(0px, -5px);
    }

    &:hover .artist-name-library{
        opacity: 100%; 
    }
`

function LibraryArtistCard(artist: Artist) {
    
    return (
        <Link to='/Artist' className='link' state={{artistProp: artist}}>
            <LikedArtistCard>
                <img src={artist.picture} className="artist-image-library" title={artist.pictureAlt} alt={artist.pictureAlt}></img>
                <p className='artist-name-library'>{artist.name}</p>   
            </LikedArtistCard>
        </Link>
    )

}

export default LibraryArtistCard