import {Playlist, Song} from '../../utils/types/type';
import styled from 'styled-components';
import {faHeart, faEllipsis, faPlay, faPause} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import songs from '../../data/songs';
import { useEffect, useState, useContext } from 'react';
import axios from 'axios';
import { artistNames } from '../../utils/functions/globalFunctions';
import { SongContext } from '../../utils/context/SongContext';
import { SongSourceContext } from '../../utils/context/SongSourceContext';

//Définition des styled-components
const MainSong = styled.div`
    display: flex;
    width:100%;
    min-width: 500px;
    height: 75px;
    background-color: #23263d;
    overflow: hidden;
    box-shadow: 3px 3px 7px rgba(0, 0, 0, 0.6);
    border-radius: 12px;
    margin-bottom: 10px;
    cursor: pointer;
    transition: all 0.1s ease-out;


    &:last-child{
        margin-bottom: 0;
    }

    &:hover{
        box-shadow: 6px 6px 12px 2px rgba(0, 0, 0, 0.4);
        transform: translate(-2px, -2px);
    }

    &:hover p{
        opacity: 80%;
    }

    &:hover h3{
        opacity: 100%;
    }
`
const MainSongCover = styled.div`
    display: flex;
    align-items: center;
    min-height: 52px;
    min-width: 52px;
    border-radius: 8px;
    overflow: hidden;
    position: relative;
`

function SearchMainSong({song, songsIds} : {song: Song, songsIds: string}) {

    const [songArtists, setSongArtists] = useState<String[]>([]);
    const [songArtistsFetch, setSongArtistsFetch] = useState<boolean>();
    const [searchMainSongsList, setSearchMainSongsList] = useState<Playlist>();
    const {songSource, setSongSource} = useContext(SongSourceContext);
    const {contextSong, setContextSong, songState, setSongState} = useContext(SongContext)

    //Fonction de contrôle depuis le clic sur une track de la playlist
    function contextControlFromTrack() {
        if(songSource !== searchMainSongsList) {
            setSongSource(searchMainSongsList!);
            if (contextSong !== song){
                setContextSong(song);
                setSongState(true);
            } 
        } else {
            if (contextSong !== song){
                setContextSong(song);
                setSongState(true);
            } else {
                setSongState(!songState);
            }
        }
    }

    //Permet de définir les icônes de play quand le son change
    function controlIcon() {
        if(song === contextSong) {
            const icon = songState ? faPause : faPlay;
            return icon;
        }
        return faPlay;
    }

    useEffect(() => {
        
        setSearchMainSongsList({
            "playlistId": -2,
            "cover": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Solid_white.svg/2048px-Solid_white.svg.png",
            "coverAlt": "Cover des résultats de la recherche",
            "creationDate": new Date(1667343600000),                    
            "duration": "00:00:00",
            "lastListeningDate": new Date(1667343600000),
            "likes": 0,
            "playlistName": "Search results",
            "songsIds": songsIds,
            "songsNumber": 4,
            "style": "Aucun",
            "user": {
                "userId": 0,
                "likedAlbums": " ",
                "likedArtists": " ",
                "likedPlaylists": " ", 
                "likedSongs": " ",
                "password": "Mocktify",
                "picture": " ",
                "pictureAlt": " ",
                "recentlyPlayed": " ",
                "userStatus": 10,
                "username": "Mocktify"
            },
            "description": "Titres recherchés"
        })

        async function fetchSongArtists(artistsIds : number[]){

            let tableSongArtists: String[] = [];

            artistsIds.forEach(async (artistId) => {
                await axios.get("http://localhost:8080/artist/" + artistId)
                .then((response) => {
                    tableSongArtists.push(response.data.name);
                    setSongArtists(tableSongArtists);
                    (tableSongArtists.length === artistsIds.length) ? setSongArtistsFetch(true) : setSongArtistsFetch(false); 
                })
                .catch(() => console.log("Couldn't fetch song artists"));
            })
        }
        fetchSongArtists(JSON.parse(song.artistsIds).artistsIds);
    }, [])

    return(<MainSong onClick={() => {setContextSong(song)}}>
            <div className="searched-song-left">
                <MainSongCover onClick={() => {contextControlFromTrack()}}>
                    <div className="search-song-control-play-button">
                        <FontAwesomeIcon icon={controlIcon()} className="play"/>
                    </div>
                    <img src={song.cover}></img>
                </MainSongCover>
                <div className="searched-song-description">
                    <h3>{song.songTitle}</h3>
                    {(songArtistsFetch) && (
                        <p>{artistNames(songArtists)}</p>
                    )}
                </div>
            </div>
            <div className="searched-song-right">
                <FontAwesomeIcon className='searched-song-control liked-song' icon={faHeart}/>
                <FontAwesomeIcon className='searched-song-control' icon={faEllipsis}/>
            </div>
        </MainSong>);
}

export default SearchMainSong