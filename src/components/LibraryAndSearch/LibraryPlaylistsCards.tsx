import React from 'react'
import styled from 'styled-components'
import artists from '../../data/artists'
import LibraryPlaylistCard from './LibraryPlaylistCard'
import { trierPlaylist } from '../../utils/functions/globalFunctions'
import { Playlist, User } from '../../utils/types/type'
import { Link } from 'react-router-dom'
import PlaylistPage from '../../views/Playlist'

const PlaylistSectionWrapper = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    height: 160px;
    flex-direction: row;
    overflow-x: auto;
`

function LibraryPlaylistsCards({playlists, username} : {playlists: Playlist[], username: string}) {

    //https://ui.dev/react-router-pass-props-to-link

    
    return(
        <PlaylistSectionWrapper>{
            trierPlaylist(playlists).map((playlist) => (
                <LibraryPlaylistCard key={playlist.playlistId}  playlist={playlist} username={username}/>
            ))
        }</PlaylistSectionWrapper>
    )
}

export default LibraryPlaylistsCards;