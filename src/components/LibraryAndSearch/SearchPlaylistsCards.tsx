import styled from 'styled-components'
import SearchPlaylistCard from './SearchPlaylistCard'
import { Playlist, User } from '../../utils/types/type'

const PlaylistSectionWrapper = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    height: 160px;
    flex-direction: row;
    overflow-x: auto;
`

function SearchPlaylistsCards({playlists} : {playlists: Playlist[]}) {
    return (
        <PlaylistSectionWrapper>{
            (playlists).map((playlist) => (<SearchPlaylistCard key={playlist.playlistId}  playlist={playlist}/>))
        }</PlaylistSectionWrapper>
    )
}

export default SearchPlaylistsCards