import { Artist } from '../../utils/types/type';
import styled from 'styled-components';
import LibraryArtistCard from './LibraryArtistCard';


//Définition des styled components
const LikedArtistsWrapper = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    height: 200px;
    flex-direction: row;
    overflow-x: auto;
`

export const LibraryArtistsCards = ({artists}: {artists: Artist[]}) => {

    return <LikedArtistsWrapper>{
        artists.map((artist) => (<LibraryArtistCard key={artist.artistId}  {...artist}/>))
    }</LikedArtistsWrapper>
}

export default LibraryArtistsCards