import styled from 'styled-components'
import SearchMainSong from './SearchMainSong';
import {Song} from '../../utils/types/type'
import { useEffect, useState } from 'react';
import axios from 'axios';

//Définition des styled components
const MainSongsWrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 300px;
    width: 100%;
    max-width: 850px;
    margin-right: 30px;
`

function getBestSearchedSongsResults(songs : Song[]) : Song[]{
    if(songs.length > 4) {
        let tabSongs : Song[] = [];
        for(let i = 0; i < 4; i++){
            tabSongs.push(songs[i]);
        }
        return tabSongs;
    }

    return songs;
}

function getSongsIds(songs: Song[]) {
    let tabIds: number[] = [];
    getBestSearchedSongsResults(songs).forEach((song) => {
        tabIds.push(song.songId);
    })
    return ("{\"songsIds\": [" + tabIds + "]}");
}   

function SearchMainSongs({songs} : {songs: Song[]}){ 
    getSongsIds(songs);
    // const songsIds : string;

    return <MainSongsWrapper>{
        getBestSearchedSongsResults(songs).map((song) => (<SearchMainSong key={song.songId} songsIds={getSongsIds(songs)} song={song}></SearchMainSong>))
    }</MainSongsWrapper>
}
export default SearchMainSongs