import { faClock, faPlay, faPause } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled from 'styled-components'
import { SongSourceContext } from '../../utils/context/SongSourceContext'
import { convertDuration, sortSongsById } from '../../utils/functions/globalFunctions'
import { User, Playlist, Song } from '../../utils/types/type'
import {useContext, useEffect, useState} from 'react'
import { Link } from 'react-router-dom'
import { SongContext } from '../../utils/context/SongContext'
import axios from 'axios'


//Définition des styled components
const LikedPlaylistCard = styled.div`
    display: flex;
    flex-drection: row;
    align-items: center;
    min-width: 250px;
    height: 130px;
    background-color: #23263d;
    border-radius: 10px;
    margin: 0 15px 0 0;
    box-shadow: 5px 5px 10px 2px rgba(0, 0, 0, 0.4);
    cursor: pointer;
    transition: all 0.1s ease-out;

    &:hover{
        box-shadow: 6px 6px 12px 2px rgba(0, 0, 0, 0.4);
        transform: translate(0px, -5px);
    }

    &:hover .playlist-album-name-library{
        opacity: 100%; 
    }
    
    &:hover .playlist-album-username-library{
        opacity: 80%; 
    }

    &:hover .playlist-album-songnumber-library{
        opacity: 60%; 
    }

    &:hover .duration-content-library{
        opacity: 100%; 
    }
`

const PlaylistDescriptionWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding-left: 10px;
    height: 100%;
    width: 100%;
    padding-right: 8px;
    padding-bottom: 14px;
`

function LibraryPlaylistCard({playlist, username} : {playlist: Playlist, username: string}) {

    //Définition des contextes
    const {songSource, setSongSource} = useContext(SongSourceContext);  
    const {songState, setSongState, setContextSong} = useContext(SongContext);

    //Définition des states
    const [playlistSongs, setPlaylistSongs] = useState<Song[]>([]);

    function playOrPausePlaylist(){
        if(!('playlistId' in songSource && songSource.playlistId === playlist.playlistId)){
            setSongSource(playlist);
            setSongState(true);
        } else {
            (songState) ? setSongState(false) : setSongState(true);
        }   
    }

    function stopPlayingWhenSongSourceChange() {
        if ('playlistId' in songSource && songSource.playlistId === playlist.playlistId){
            const icon = (songState) ? faPause : faPlay;
            return icon;
        } else {
            return faPlay;
        }
    }

    useEffect(() => {
        async function fetchPlaylistSongs(songsIds: number[]) {

            let tableSongs : Song[] = [];
    
            songsIds.forEach(async (songId) => {
                await axios.get("http://localhost:8080/song/" + songId)
                .then((response) => {
                    tableSongs.push(response.data);
                    if (tableSongs.length === songsIds.length){
                        setPlaylistSongs(sortSongsById(tableSongs));
                    }
                })
                .catch(() => console.log("Couldn't fetch playlist songs"))
            })
        }
        fetchPlaylistSongs(JSON.parse(playlist.songsIds).songsIds);
    }, [])
    
    return (
        <LikedPlaylistCard>
            <div className="playlist-album-card-cover" onClick={() => {setSongSource(playlist); setContextSong(playlistSongs[playlistSongs.length - 1])}}>
                <div className='playlist-album-play-button'>
                    <FontAwesomeIcon icon={stopPlayingWhenSongSourceChange()} onClick={() => playOrPausePlaylist()} className="play"/>
                </div>
                <img src={playlist.cover} className="playlist-album-image-library"/>
            </div>
            <Link to='/Playlist' className='link' state={{playlistProp: playlist}}>
                <PlaylistDescriptionWrapper>
                    <p className='playlist-album-name-library'>{playlist.playlistName}</p>
                    <p className='playlist-album-username-library'>{username}</p>
                    <p className='playlist-album-songnumber-library'>{"Playlist - " + playlist.songsNumber + " songs"}</p>
                    <div className="duration-container-library">
                        <div className="duration-library">
                            <p className="duration-library duration-content-library" > <FontAwesomeIcon icon={faClock} />{"  " + convertDuration(playlist.duration)}</p>
                        </div>
                    </div>
                </PlaylistDescriptionWrapper>
            </Link>
        </LikedPlaylistCard>
    )
}

export default LibraryPlaylistCard