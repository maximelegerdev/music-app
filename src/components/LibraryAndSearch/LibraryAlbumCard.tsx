import { faClock, faPause, faPlay } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import styled from 'styled-components'
import "../../styles/library.scss";
import { useState, useEffect, useContext } from 'react'
import axios from 'axios'
import { artistNames, convertDuration } from '../../utils/functions/globalFunctions'
import { Album } from '../../utils/types/type'
import { SongContext } from '../../utils/context/SongContext'
import { SongSourceContext } from '../../utils/context/SongSourceContext'
import { Link } from 'react-router-dom';

//Définition des styled components
const LikedAlbumCard = styled.div`
    display: flex;
    flex-drection: row;
    align-items: center;
    min-width: 250px;
    height: 130px;
    background-color: #23263d;
    border-radius: 10px;
    margin: 0 15px 0 0;
    box-shadow: 5px 5px 10px 2px rgba(0, 0, 0, 0.4);
    cursor: pointer;
    transition: all 0.1s ease-out;

    &:hover{
        box-shadow: 6px 6px 12px 2px rgba(0, 0, 0, 0.4);
        transform: translate(0px, -5px);
    }

    &:hover .playlist-album-name-library{
        opacity: 100%; 
    }

    &:hover .playlist-album-username-library{
        opacity: 80%; 
    }

    &:hover .playlist-album-songnumber-library{
        opacity: 60%; 
    }

    &:hover .duration-content-library{
        opacity: 100%; 
    }
`

const AlbumDescriptionWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding-left: 10px;
    height: 100%;
    width: 100%;
    padding-right: 10px;
    padding-bottom: 10px;
`

function LibraryAlbumCard ({album} : {album : Album}) {

    //Définition des states
    const [albumArtists, setAlbumArtists] = useState<String[]>([]);
    const [albumArtistsFetch, setAlbumArtistsFetch] = useState<boolean>();

    //Définition des contextes
    const {songSource, setSongSource} = useContext(SongSourceContext);
    const {setContextSong, songState, setSongState} = useContext(SongContext);

    //Définition des fonctions
    function playOrPauseAlbum(){
        if(!('albumId' in songSource && songSource.albumId === album.albumId)){
            setSongSource(album);
            setSongState(true);
        } else {
            (songState) ? setSongState(false) : setSongState(true);
        }   
    }

    function stopPlayingWhenSongSourceChange() {
        if ('albumId' in songSource && songSource.albumId === album.albumId){
            const icon = (songState) ? faPause : faPlay;
            return icon;
        } else {
            return faPlay;
        }
    }

    useEffect(() => {
        async function fetchAlbumArtists(artistsIds : number[]) {
            
            let tableAlbumArtists : String[] = [];

            artistsIds.forEach(async (artistId) => {
                await axios.get("http://localhost:8080/artist/" + artistId)
                .then((response) => {
                    tableAlbumArtists.push(response.data.name);
                    setAlbumArtists(tableAlbumArtists);
                    (tableAlbumArtists.length === artistsIds.length) ? setAlbumArtistsFetch(true) : setAlbumArtistsFetch(false);
                })
                .catch(() => console.log("Couldn't fetch album artists"));
            })
        }
        fetchAlbumArtists(JSON.parse(album.artistsIds).artistsIds);
    }, [])

    return(
        <LikedAlbumCard>
            <div className="playlist-album-card-cover"  onClick={() => {setSongSource(album); setContextSong(album.songs[0])}}>
                <div className='playlist-album-play-button'>
                    <FontAwesomeIcon icon={stopPlayingWhenSongSourceChange()} onClick={() => playOrPauseAlbum()} className="play"/>
                </div>
                <img src={album.cover} className="playlist-album-image-library"></img>
            </div>
            <Link to='/Album' className='link' state={{albumProp : album}}>
                <AlbumDescriptionWrapper>
                    <p className='playlist-album-name-library'>{album.albumTitle}</p>
                    {(albumArtistsFetch) && (
                        <p className='playlist-album-username-library'>{artistNames(albumArtists)}</p>
                    )}
                    <p className='playlist-album-songnumber-library'>{album.albumType + " - " + album.songsNumber + " songs"}</p>
                    <div className="duration-container-library">
                        <div className="duration-library">
                            <p className="duration-library duration-content-library" > <FontAwesomeIcon icon={faClock} />{"  " + convertDuration(album.duration)}</p>
                        </div>
                    </div>
                </AlbumDescriptionWrapper>
            </Link>
        </LikedAlbumCard>
    )
}

export default LibraryAlbumCard;