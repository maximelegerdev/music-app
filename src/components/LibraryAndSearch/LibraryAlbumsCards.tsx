import styled from 'styled-components'
import LibraryAlbumCard from './LibraryAlbumCard'
import { Album } from '../../utils/types/type'

//Définition des styled components
const AlbumCardWrapper = styled.div`
    display: flex;
    align-items: center;
    width: 100%;
    height: 160px;
    flex-direction: row;
    overflow-x: auto;
`

function LibraryAlbumsCards ({albums} : {albums : Album[]}) {
    return(
        <AlbumCardWrapper>{
            albums.map((album) => (<LibraryAlbumCard key={album.albumId} album={album}/>))
        }</AlbumCardWrapper>
    )
}

export default LibraryAlbumsCards;