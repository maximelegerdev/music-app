import React, { useContext } from "react";
import styled from "styled-components";
import '../../styles/PlaylistPage/trackInList.scss'
import {useState, useEffect} from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faEllipsis, faHeart, faPause, faPlay } from "@fortawesome/free-solid-svg-icons";
import { Song, Artist, Playlist } from "../../utils/types/type";
import axios from "axios";
import { artistNamesToJSX } from "../../utils/functions/globalFunctions";
import { SongSourceContext } from "../../utils/context/SongSourceContext";
import { SongContext } from "../../utils/context/SongContext";
import { UserContext } from "../../utils/context/UserContext";



// const TrackInListWrapper = styled.div`
//     display: flex;
//     flex-direction: row;
//     align-items: center;
//     padding : 13px 13px;
//     height: 55px;
//     width: 100%;
//     background-color: #23263d;
//     transition: all 0.1s ease-out;
//     cursor: pointer;
//     &:hover{
//         background-color: #595b99;
//     }
// `

type TrackInListWrapperProps = {
    dominantColor: string;
}

const TrackInListWrapper = styled.div<TrackInListWrapperProps>`
    display: flex;
    flex-direction: row;
    align-items: center;
    padding : 13px 30px;
    height: 65px;
    width: 100%;
    min-width: 450px;
    transition: all 0.1s ease-out;
    cursor: pointer;

    &:hover{
        background-color: ${props => props.dominantColor};
    }
`

// &:hover{
//     background-color: #23263d;
// }

const CoverInList = styled.div`
    display: flex;
    align-items: center;
    min-height: 65px;
    min-width: 65px;
    border-radius: 10px;
    overflow: hidden;
    position: relative;

    .track-in-list-play-button{
        display: flex;
        align-items: center;
        justify-content: center;
        min-width: 65px;
        min-height: 65px;
        z-index: 1;
        position: absolute;
        background-color: #00000000;
        transition: background-color 0.1s ease-out;
    

        &:hover{
            background-color: #00000040;

            .play{
                color: white;
            }
        }

        .play{
            height: 22px;
            width: 22px;
            color: transparent;
            transition: all 0.1s ease-out;
            margin-left: 3px;

            &:hover{
                opacity: 0.8;
            }
        }
    }
`

const NamesInList = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    justify-content: center;
    padding: 0px 0 2px 15px;
    line-height: 0;
`

const ControlsInList = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    height: 100%;
    padding:  0 45px 0 0 ;
`


function TrackInList ({song, likedSongsIds, playlist, dominantColor} : {song: Song, likedSongsIds: number[], playlist: Playlist, dominantColor: string}) {
    
    //Défintion des states
    const [isLiked, setIsLiked] = useState("control-icon-in-list");
    const [artistsFetch, setArtistsFetch] = useState(false);
    const [artists, setArtists] = useState<Artist[]>([]);

    //Définition des contextes
    const {user} = useContext(UserContext);
    const {contextSong, setContextSong, songState, setSongState} = useContext(SongContext);
    const {songSource, setSongSource} = useContext(SongSourceContext);

    //Définition des fonctions
    function changeLikedValue () {
        if(isLiked !== "control-icon-in-list"){
            setIsLiked("control-icon-in-list")
        } else {
            setIsLiked("control-icon-in-list liked");
        }
    }

    function isSongLiked(song : Song, likedSongIds: number[]){
        let liked: boolean = false;
        likedSongIds.forEach((likedSongId) => {
            (likedSongId === song.songId) ? liked = true : liked = liked;
        })
        if(liked) {
            setIsLiked("control-icon-in-list liked");
        }
    }

    //Permet de définir les icônes de play quand le son change
    function controlIcon() {
        if(song === contextSong) {
            const icon = songState ? faPause : faPlay;
            return icon;
        }
        return faPlay;
    }


    //Fonction de contrôle depuis le clic sur une track de la playlist
    function contextControlFromTrack() {
        if(songSource !== playlist) {
            setSongSource(playlist);
            if (contextSong !== song){
                setContextSong(song);
                setSongState(true);
            } 
        } else {
            if (contextSong !== song){
                setContextSong(song);
                setSongState(true);
            } else {
                setSongState(!songState);
            }
        }
    }

    //Définition du useEffect
    useEffect(() => {
        async function fetchSongArtists(artistsIds: number[]) {

        let tableArtists: Artist[] = [];
            
            artistsIds.forEach(async (artistId) => {
                await axios.get("http://localhost:8080/artist/" + artistId)
                .then((response) =>{
                    tableArtists.push(response.data);
                    setArtists(tableArtists);
                    tableArtists.length === artistsIds.length ? setArtistsFetch(true) : setArtistsFetch(false);
                }) 
                .catch(() => console.log("Couldn't fetch artists"))  
            })
        }

        fetchSongArtists(JSON.parse(song.artistsIds).artistsIds)
    }, [])

    

    return (
        <TrackInListWrapper onLoad={() => isSongLiked(song, likedSongsIds)} dominantColor={dominantColor}>
            <CoverInList onClick={() => {contextControlFromTrack()}}>
                <div className="track-in-list-play-button">
                    <FontAwesomeIcon icon={controlIcon()} className="play"/>
                </div>
                <img alt={song.coverAlt} title={song.coverAlt} className="cover-image-in-list" src={song.cover}></img>
            </CoverInList>
            <NamesInList>
                <h3 className="song-title-in-list">{song.songTitle}</h3>
                {artistsFetch && <p className="artist-name-in-list">{artistNamesToJSX(artists)}</p>}
            </NamesInList>
            <ControlsInList>
                <FontAwesomeIcon onClick={() => changeLikedValue()} className={isLiked} icon={faHeart}/>
                <FontAwesomeIcon className="control-icon-in-list" icon={faEllipsis}/>
            </ControlsInList>
        </TrackInListWrapper>
    )
}

export default TrackInList;