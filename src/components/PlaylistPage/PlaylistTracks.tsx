import styled from "styled-components"
import { Playlist, Song, User } from "../../utils/types/type"
import TrackInList from "./TrackInList"
import { Artist } from "../../utils/types/type"
import {useState, useEffect} from 'react'
import axios from "axios"


//Définition des styled-components
const PlaylistTracksWrapper = styled.div`
    display: flex;
    overflow-x: hidden;
    flex-direction: column;
    width: 100%;
`

type PlaylistTrackProps = {
    songs : Song[],
    playlist: Playlist
    dominantColor: string;
}

function PlaylistTracks({songs, playlist, dominantColor} : PlaylistTrackProps) {
    
    //Définition des states
    const [user, setUser] = useState<User | null>(null);
    const [userFetch, setUserFetch] = useState(false)

    //Définition du useEffect
    useEffect(() => {
        async function fetchUser(){
            await axios.get('http://localhost:8080/user/1')
            .then((response) => {
                setUser(response.data);
                setUserFetch(true);
            })
        }
        fetchUser();
    }, [])

    return(
        <PlaylistTracksWrapper>
            {user && songs.map((song) => (
                <TrackInList key={song.songId} dominantColor={dominantColor} playlist={playlist} song={song} likedSongsIds={JSON.parse(user.likedSongs).likedSongsIds}></TrackInList>
            ))}
        </PlaylistTracksWrapper>
    )
} 


export default PlaylistTracks