import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import '../styles/track.scss'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsis, faHeart, faPlay, faPause } from "@fortawesome/free-solid-svg-icons";
import { SongContext } from "../utils/context/SongContext";
import { Album, Playlist, Song } from "../utils/types/type";
import { getAudioAndArtistNamesForSong } from "../utils/functions/globalFunctions";
import { SongSourceContext } from "../utils/context/SongSourceContext";
import { UserContext } from "../utils/context/UserContext";

const TrackWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    padding : 15px 8px;
    margin : 11px 0;
    border-radius: 12px;
    height: 60px;
    min-width: 495px;
    width: 46.5%;
    background-color: #23263d;
    box-shadow: 5px 5px 10px 2px rgba(0, 0, 0, 0.4);
    transition: all 0.1s ease-out;
    cursor: pointer;

    &:hover{
        box-shadow: 6px 6px 12px 2px rgba(0, 0, 0, 0.4);
        transform: translate(-2px, -2px);
    }

    &:hover .song-title{
        opacity: 1;
    }

    &:hover .artist-name{
        opacity: 0.8;
    }
`

const Cover = styled.div`
    display: flex;
    align-items: center;
    min-height: 74px;
    min-width: 74px;
    margin-left: 1px;
    border-radius: 8px;
    overflow: hidden;
    
    position: relative;

    .track-play-button{
        display: flex;
        align-items: center;
        justify-content: center;
        min-width: 74px;
        min-height: 74px;
        z-index: 1;
        position: absolute;
        background-color: #00000000;
        transition: background-color 0.1s ease-out;
        &:hover{
            background-color: #00000040;

            .play{
                color: white;
            }
        }

        .play{
            height: 25px;
            width: 25px;
            color: transparent;
            transition: all 0.1s ease-out;
            margin-left: 3px;

            &:hover{
                opacity: 0.8;
            }
        }
    }
`

const Names = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    justify-content: center;
    padding: 0px 12px;

`

const Controls = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    height: 100%;
`



function Track({song,} : {song: Song}) {

    //Définition des contextes
    const {user} = useContext(UserContext);
    const {contextSong, setContextSong, songState, setSongState} = useContext(SongContext);
    const {songSource, setSongSource} = useContext(SongSourceContext);
    const [recentlyPlayedPlaylist, setRecentlyPlayedPlaylist] = useState<Playlist>()

    //Fonction de contrôle depuis le clic sur une track de la playlist
        function contextControlFromTrack() {
        if(songSource !== recentlyPlayedPlaylist) {
            setSongSource(recentlyPlayedPlaylist!);
            if (contextSong !== song){
                setContextSong(song);
                setSongState(true);
            } 
        } else {
            if (contextSong !== song){
                setContextSong(song);
                setSongState(true);
            } else {
                setSongState(!songState);
            }
        }
    }

    //Permet de définir les icônes de play quand le son change
    function controlIcon() {
        if(song === contextSong) {
            const icon = songState ? faPause : faPlay;
            return icon;
        }
        return faPlay;
    }

    useEffect(() => {
            setRecentlyPlayedPlaylist({
            "playlistId": -1,
            "cover": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Solid_white.svg/2048px-Solid_white.svg.png",
            "coverAlt": "Cover des titres récemment joués",
            "creationDate": new Date(1667343600000),                    
            "duration": "00:10:38",
            "lastListeningDate": new Date(1667343600000),
            "likes": 0,
            "playlistName": "Recently Played",
            "songsIds": user!.recentlyPlayed,
            "songsNumber": 4,
            "style": "Aucun",
            "user": user!,
            "description": "Titres récemment joués"
        })
    }, [])
    
    return (
        <TrackWrapper>
            <Cover>
                <div className="track-play-button" onClick={() => {contextControlFromTrack()}}>
                    <FontAwesomeIcon icon={controlIcon()} className="play"/>
                </div>
                <img alt={song.coverAlt} title={song.coverAlt} className="cover-image" src={song.cover}></img>
            </Cover>
            <Names>
                <h3 className="song-title">{song.songTitle}</h3>
                <p className="artist-name">{getAudioAndArtistNamesForSong(song).artist}</p>
            </Names>
            <Controls>
                <FontAwesomeIcon className="control-icon-liked" icon={faHeart}/>
                <FontAwesomeIcon className="control-icon" icon={faEllipsis}/>
            </Controls>
        </TrackWrapper>
    )    
}

export default Track;