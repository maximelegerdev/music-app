import styled from "styled-components";
import {Album, Song, User} from '../../utils/types/type';
import TrackInAlbum from "../AlbumPage/TrackInAlbum";
import {Artist} from "../../utils/types/type";
import {useState, useEffect} from 'react';
import axios from "axios";

//Définition des styled-components
const AlbumTracksWrapper = styled.div`
    display: flex;
    overflow-x: hidden;
    flex-direction: column;
    width: 100%;
`

type AlbumTracksProps = {
    songs : Song[],
    album: Album,
    dominantColor: string;
}

function AlbumTracks({songs, album, dominantColor}: AlbumTracksProps) {

    //Définition des states
    const [user, setUser] = useState<User | null>(null);

    //Définition du useEffect
    useEffect(() => {
        async function fetchUser(){
            await axios.get('http://localhost:8080/user/1')
            .then((response) => {
                setUser(response.data)
            })
        }
        fetchUser();
    }, [])
    
    return(
        <AlbumTracksWrapper>
            {(user) && songs.map((song) => (
                <TrackInAlbum key={song.songId} album={album} song={song} likedSongsIds={JSON.parse(user.likedSongs).likedSongsIds} dominantColor={dominantColor}></TrackInAlbum>
            ))}            
        </AlbumTracksWrapper>
    )
}

export default AlbumTracks