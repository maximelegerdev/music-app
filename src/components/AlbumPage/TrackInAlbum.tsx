import { dom } from "@fortawesome/fontawesome-svg-core"
import { faEllipsis, faHeart, faPause, faPlay } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import axios from "axios"
import '../../styles/PlaylistPage/trackInList.scss'
import { useContext, useEffect, useState } from "react"
import styled from "styled-components"
import { SongContext } from "../../utils/context/SongContext"
import { SongSourceContext } from "../../utils/context/SongSourceContext"
import { UserContext } from "../../utils/context/UserContext"
import { artistNamesToJSX } from "../../utils/functions/globalFunctions"
import { Album, Artist, Song } from "../../utils/types/type"

type TrackInAlbumWrapperProps = {
    dominantColor: string;
}

const TrackInAlbumWrapper = styled.div<TrackInAlbumWrapperProps>`
    display: flex;
    flex-direction: row;
    align-items: center;
    padding : 13px 0 13px 15px;
    height: 65px;
    width: 100%;
    min-width: 450px;
    transition: all 0.1s ease-out;
    cursor: pointer;

    &:hover{
        background-color: ${props => props.dominantColor};
    }
`

// &:hover{
//     background-color: #23263d;
// }

const CoverInAlbum = styled.div`
    display: flex;
    align-items: center;
    min-height: 65px;
    min-width: 65px;
    border-radius: 10px;
    overflow: hidden;
    position: relative;

    .track-in-list-play-button{
        display: flex;
        align-items: center;
        justify-content: center;
        min-width: 65px;
        min-height: 65px;
        z-index: 1;
        position: absolute;
        background-color: #00000000;
        transition: background-color 0.1s ease-out;


        &:hover{
            background-color: #00000040;

            .play{
                color: white;
            }
        }

        .play{
            height: 22px;
            width: 22px;
            color: transparent;
            transition: all 0.1s ease-out;
            margin-left: 3px;

            &:hover{
                opacity: 0.8;
            }
        }
    }
`

const NamesInAlbum = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    justify-content: center;
    padding: 0px 0 2px 15px;
    line-height: 0;
`

const ControlsInAlbum = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
    height: 100%;
    padding:  0 30px 0 0 ;
`


function TrackInAlbum ({song, likedSongsIds, album, dominantColor}: {song: Song, likedSongsIds: number[], album: Album, dominantColor: string}) {

    //Définition des states
    const [isLiked, setIsLiked] = useState("control-icon-in-list");
    const [artists, setArtists] = useState<Artist[]>([]);

    //Définition des contextes
     //Définition des contextes
     const {user} = useContext(UserContext);
     const {contextSong, setContextSong, songState, setSongState} = useContext(SongContext);
     const {songSource, setSongSource} = useContext(SongSourceContext);

    //Définition des fonctions 
    function changeLikedValue () {
        console.log(isLiked);
        if(isLiked !== "control-icon-in-list"){
            setIsLiked("control-icon-in-list")
        } else {
            setIsLiked("control-icon-in-list liked");
        }
    }

    function isSongLiked(song : Song, likedSongIds: number[]){
        let liked: boolean = false;
        likedSongIds.forEach((likedSongId) => {
            if(likedSongId === song.songId) {
                liked = true;
            }
        })
        if(liked) {
            setIsLiked("control-icon-in-list liked");
        }
    }

    function contextControlFromTrack() {
        if(songSource !== album) {
            setSongSource(album);
            if (contextSong !== song){
                setContextSong(song);
                setSongState(true);
            } 
        } else {
            if (contextSong !== song){
                setContextSong(song);
                setSongState(true);
            } else {
                setSongState(!songState);
            }
        }
    }

    //Permet de définir les icônes de play quand le son change
    function controlIcon() {
        if(song === contextSong) {
            const icon = songState ? faPause : faPlay;
            return icon;
        }
        return faPlay;
    }


    //Définition du useEffect
    useEffect(() => {
        async function fetchSongArtists(artistsIds: number[]) {

        let tableArtists: Artist[] = [];
            
            artistsIds.forEach(async (artistId) => {
                await axios.get("http://localhost:8080/artist/" + artistId)
                .then((response: { data: Artist }) =>{
                    tableArtists.push(response.data);
                    if(tableArtists.length === artistsIds.length){
                        setArtists(tableArtists);
                    } 
                }) 
                .catch(() => console.log("Couldn't fetch artists"))  
            })
        }

        fetchSongArtists(JSON.parse(song.artistsIds).artistsIds)
    }, [])

    return(
        <TrackInAlbumWrapper onLoad={() => isSongLiked(song, likedSongsIds)} dominantColor={dominantColor}>
            <NamesInAlbum onClick={() => {contextControlFromTrack()}}>
                <h3 className="song-title-in-list">{song.songTitle}</h3>
                {artists.length > 0 && <p className="artist-name-in-list">{artistNamesToJSX(artists)}</p>}
            </NamesInAlbum>
            <ControlsInAlbum>
                <FontAwesomeIcon onClick={() => changeLikedValue()} className={isLiked} icon={faHeart}/>
                <FontAwesomeIcon className="control-icon-in-list" icon={faEllipsis}/>
            </ControlsInAlbum>
        </TrackInAlbumWrapper>
    )

}

export default TrackInAlbum