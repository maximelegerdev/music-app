import React from "react";
import playlists from '../data/playlists'
import songs from '../data/songs'
import styled from "styled-components";
import '../styles/playlistCard.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClock } from "@fortawesome/free-solid-svg-icons";

const PlaylistCardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    border-radius: 10px;
    margin: 10px 25px 0 0 ;
    height: 189px;
    width: 300px;
    padding: 8px;
    background-color: #23263d;
    box-shadow: 3px 3px 5px 2px rgba(0, 0, 0, 0.4);
    cursor: pointer;
    transition: all 0.1s ease-out;

    &:hover{
        box-shadow: 3px 6px 8px 2px rgba(0, 0, 0, 0.4);
        transform: translate(0px, -3px);
    }

    &:hover .playlist-title, &:hover .duration-content{
        opacity: 1;
    }

    &:hover .playlist-user{
        opacity: 80%;
    }

    &:hover .description{
        opacity: 60%;
    }

`

const CoverWrapper = styled.div`
    display: flex;
    flex-direction: row;
    height: 55px;
    width: 100%;
    margin: 6px 3px 0;
`

const TitleUserWrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 60px;
    width: 100%;
    margin: 4px 3px 0;
`
const DescDurationWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    margin: 0px 3px 0;
    height: 70px;
    font-size: 8px;
`

type Song = {
    id: number, 
    artist: string, 
    songTitle: string, 
    audio: string, 
    cover: string,
    alt: string,
    duration: string
}

type Playlist = {
    id: number, 
    name: string, 
    idUser: number, 
    idsSongs: number[], 
    nbrSongs: number,
    cover: string,
    duration: string,
    creationDate: string,
    description: string,
    isFavorite: boolean
}

function PlaylistCard() {
    
    function getPlaylistSongs (playlist: Playlist, songs: Song[], playlistSongs: Song[]) {
        songs.forEach(song => {
            playlist.idsSongs.forEach(idSong => {
                if(song.id === idSong){
                    playlistSongs.push(song);
                }
            })
        });
        return playlistSongs;
    }

    let playlistSongs: Song[] = [];


    return(
        <PlaylistCardWrapper>
            <CoverWrapper>
                <img title={getPlaylistSongs(playlists[0], songs, playlistSongs)[0].alt} 
                    alt={getPlaylistSongs(playlists[0], songs, playlistSongs)[0].alt} 
                    className="cover-playlist-minimal" src={getPlaylistSongs(playlists[0], songs, playlistSongs)[0].cover}></img>
                <img title={getPlaylistSongs(playlists[1], songs, playlistSongs)[1].alt} 
                    alt={getPlaylistSongs(playlists[1], songs, playlistSongs)[1].alt} 
                    className="cover-playlist-minimal" src={getPlaylistSongs(playlists[1], songs, playlistSongs)[1].cover}></img>
                <img title={getPlaylistSongs(playlists[2], songs, playlistSongs)[2].alt} 
                    alt={getPlaylistSongs(playlists[2], songs, playlistSongs)[2].alt} 
                    className="cover-playlist-minimal" src={getPlaylistSongs(playlists[2], songs, playlistSongs)[2].cover}></img>
                <img title={getPlaylistSongs(playlists[3], songs, playlistSongs)[3].alt} 
                    alt={getPlaylistSongs(playlists[3], songs, playlistSongs)[3].alt} 
                    className="cover-playlist-minimal" src={getPlaylistSongs(playlists[3], songs, playlistSongs)[3].cover}></img>
                <img title={getPlaylistSongs(playlists[3], songs, playlistSongs)[0].alt} 
                    alt={getPlaylistSongs(playlists[3], songs, playlistSongs)[0].alt} 
                    className="cover-playlist-minimal" src={getPlaylistSongs(playlists[3], songs, playlistSongs)[0].cover}></img>
            </CoverWrapper>
            <TitleUserWrapper>
                <h3 className="playlist-title">{playlists[0].name}</h3>
                <p className="playlist-user">it's june</p>
            </TitleUserWrapper>
            
            <DescDurationWrapper>
                <p className="description">{playlists[0].description}</p>
                <div className="duration-container">
                    <div className="duration">
                        <p className="duration duration-content" > <FontAwesomeIcon icon={faClock} /> 1h47</p>
                    </div>
                </div>
            </DescDurationWrapper>
        </PlaylistCardWrapper>
    )

}

export default PlaylistCard