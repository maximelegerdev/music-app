import axios from 'axios'
import { useEffect, useState } from 'react'
import styled from 'styled-components'
import '../../styles/ArtistPage/artistPage.scss'
import { sliceTopSongs, sortSongsByStreams } from '../../utils/functions/globalFunctions'
import { Song } from '../../utils/types/type'
import TopTrack from './TopTrack'

//Définition des styled-components
const TopTracksWrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: fit-content;
    width: 100%;
    min-width: 500px;
    border-radius: 15px;
    overflow: hidden;
    box-shadow: 4px 4px 10px rgba(0, 0, 0, 0.6);
`
function TopTracks({dominantColor, songIds}: {dominantColor: string, songIds: number[]}){

    const [popularSongs, setPopularSongs] = useState<Song[]>([])
    let counter = 0;

    useEffect(() => {
        async function fetchTopSongs(songsIds: number[]) {
            let tablePopularSongs: Song[] = [];
            songsIds.forEach((songId) => {
                axios.get("http://localhost:8080/song/"+ songId)
                .then((response) => {
                    tablePopularSongs.push(response.data)
                    if(songsIds.length === tablePopularSongs.length){
                        setPopularSongs(sortSongsByStreams(sliceTopSongs(tablePopularSongs)));
                    }
                })
            })
        }

        fetchTopSongs(songIds);
    }, [])  
    console.log(popularSongs);
    return(
        <TopTracksWrapper>
            {popularSongs.map((song) => {
                counter++;
                return <TopTrack key={song.songId} dominantColor={dominantColor} rank={counter} song={song}/>
            })}
        </TopTracksWrapper>
    )
}

export default TopTracks