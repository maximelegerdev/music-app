import styled from "styled-components";
import { Song } from "../../utils/types/type";
import { Artist } from "../../utils/types/type";
import '../../styles/ArtistPage/artistPage.scss'
import { useEffect, useState } from "react";
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsis, faHeart } from "@fortawesome/free-solid-svg-icons";

//Définition des types des props
type TopTrackWrapperProps = {
    dominantColor: string;
}

type TopTrackProps = {
    dominantColor: string,
    rank: number,
    song: Song;
}

//Définition des styled-components
const TopTrackWrapper = styled.div<TopTrackWrapperProps>`
    display: grid;
    grid-template-columns: 1.8fr 1fr 1fr;
    width: 100%;
    height: 72px;
    cursor: pointer;
    transition: all 0.05s ease-out;
    background-color: #23263d;

    :hover{
        background-color: ${props => props.dominantColor};
    }
`

const LeftWrapper = styled.div`
    grid-column: 1/2;
    grid-row: 1/2; 
    display: flex;
    align-items: center;
    padding: 0 0 0 15px;
`

const CenterWrapper = styled.div`
    grid-column: 2/3;
    grid-row: 1/2;
    display: flex;
    align-items: center;
    justify-content: flex-end;
`

const RightWrapper = styled.div`
    grid-column: 3/4;
    grid-row: 1/2;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    padding: 0 23px 0 0;
`


function TopTrack({dominantColor, rank, song} : TopTrackProps){

    return (
        <TopTrackWrapper dominantColor={dominantColor}>
            <LeftWrapper>
                <p className="rank">{rank}</p>
                <img src={song?.cover} title={song?.coverAlt} alt={song?.coverAlt} className="popular-song-cover" />
                <h3 className="popular-song-title">{song?.songTitle}</h3>
                {song?.isExplicit && <div title="Morceau explicite" className="explicit">E</div>}
            </LeftWrapper>
            <CenterWrapper>
                <p className="popular-song-streams">{song?.streams}</p>
            </CenterWrapper>
            <RightWrapper>
                <FontAwesomeIcon icon={faHeart} className="popular-song-like"/>
                <FontAwesomeIcon icon={faEllipsis} className="popular-song-ellipsis"/>
                <p className="popular-song-duration">{song?.duration.substring(3, 8)}</p>
            </RightWrapper>
        </TopTrackWrapper>
    )
}

export default TopTrack 