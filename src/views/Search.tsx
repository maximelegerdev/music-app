import React, { useEffect, useState } from 'react'
import styled from 'styled-components';
import '../styles/search.scss'
import { faSearch, faClose } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {useRef} from 'react';
import axios from 'axios';
import {Artist, Song, Album, Playlist} from '../utils/types/type'
import '../styles/library.scss';
import LibraryArtistsCards from '../components/LibraryAndSearch/LibraryArtistsCards';
import LibraryAlbumsCards from '../components/LibraryAndSearch/LibraryAlbumsCards';
import SearchPlaylistsCards from '../components/LibraryAndSearch/SearchPlaylistsCards';
import SearchMainSongs from '../components/LibraryAndSearch/SearchMainSongs';
import TrackInList from '../components/PlaylistPage/TrackInList';


//Définition des styled components
const SearchWrapper = styled.div`
    display : flex;
    flex-direction: column;
    align-items: center;
    padding: 0 30px;
    padding-right: 0px;
    margin-right: 0;
    background-color : #161728;
    color: #fff;
    position: relative;
    width: 100%;
    height: 100%;
    overflow-y: scroll;
    overflow-x: hidden;
`

const SearchBarGroup = styled.div`
    display: flex;
    align-items: center;
    width: 500px;
    height: 75px;
    margin-top: 30px;
    overflow: visible;
`

const SearchGroup = styled.div`
    margin-bottom: 5px;
`

const SearchGroupsWrapper = styled.div`
    display : flex;
    flex-direction: column;
    padding 20px 30px;
    background-color : #161728;
    color: #fff;
    position: relative;
    width: 100%;
`


function Search() {

    //Définition des refs
    const clearRef = useRef(document.createElement("div"));
    const inputRef = useRef(document.createElement("input"));
    const resultsTitleRef = useRef(document.createElement("h2"));

    //Définition des states
    const [inputContent, setInputContent] = useState("");
    const [searchedArtists, setSearchedArtists] = useState<Artist[]>([]);
    const [searchedSongs, setSearchedSongs] = useState<Song[]>([]);
    const [searchedPlaylists, setSearchedPlaylists] = useState<Playlist[]>([]);
    const [searchedAlbums, setSearchedAlbums] = useState<Album[]>([]);

    //Définition des fonctions
    function applyInputChanges() {
        setInputContent(inputRef.current.value);
        if(inputRef.current.value !== "") {
            clearRef.current.style.display = "flex";
            resultsTitleRef.current.style.display = "flex";
        } else {
            clearRef.current.style.display = "none";
            resultsTitleRef.current.style.display = "none";
            setSearchedArtists([]);
            setSearchedPlaylists([]);
            setSearchedAlbums([]);
            setSearchedSongs([]);
        } 
    }

    function clearSearchBar () {
        inputRef.current.value = "";
        setInputContent("");
        applyInputChanges();
    }

    //Fonctions de fetch
    async function fetchSearchedArtists(searchContent: string){
        await axios.get("http://localhost:8080/artist/search/" + searchContent)
        .then((response) => {
            setSearchedArtists(response.data);
        })
        .catch(() => console.log("Couldn't fetch searched artists"))
    }

    async function fetchSearchedSongs(searchContent: string){
        await axios.get("http://localhost:8080/song/search/" + searchContent)
        .then((response) => {
            setSearchedSongs(response.data);
        })
        .catch(() => console.log("Couldn't fetch searched songs"))
    }

    async function fetchSearchedAlbums(searchContent: string){
        await axios.get("http://localhost:8080/album/search/" + searchContent)
        .then((response) => {
            setSearchedAlbums(response.data);
        })
        .catch(() => console.log("Couldn't fetch searched albums"))
    }

    async function fetchSearchedPlaylists(searchContent: string){
        await axios.get("http://localhost:8080/playlist/search/" + searchContent)
        .then((response) => {
            setSearchedPlaylists(response.data);
        })
        .catch(() => console.log("Couldn't fetch searched playlists"))
    }

    //Définition du useEffect
    useEffect(() => {
        async function research(searchContent: string){
            fetchSearchedAlbums(searchContent);
            fetchSearchedSongs(searchContent);
            fetchSearchedPlaylists(searchContent);
            fetchSearchedArtists(searchContent);
        }

        (inputRef.current.value !== "") ? research(inputRef.current.value) : console.log("Aucune recherche");

    }, [inputRef.current.value])

    return (
        <SearchWrapper>
            <SearchBarGroup>
                <div className='search-icon search' ><FontAwesomeIcon icon={faSearch} /></div>
                <input ref={inputRef} className="search-bar" onChange={() => applyInputChanges()} type="text" placeholder='Que souhaitez-vous écouter ?'/>
                <div ref={clearRef} className='search-icon clear' onClick={() => clearSearchBar()}><FontAwesomeIcon icon={faClose} /></div>
            </SearchBarGroup>
            <SearchGroupsWrapper>
                <h2 ref={resultsTitleRef} className='results-title'>{'Résultats pour "' + inputContent + '"'}</h2>
                <div className="main-results-wrapper">
                    {(searchedArtists.length > 0) && (
                        <div className="main-artist">
                            <img src={searchedArtists[0].picture} 
                            alt={searchedArtists[0].picture}
                            title={searchedArtists[0].pictureAlt}
                            className="main-artist-image"/>
                            <div className="main-artist-description">
                                <p className='main-artist-name'>{searchedArtists[0].name}</p>
                                <p className='main-artist-label'>ARTISTE</p>
                            </div>
                        </div>
                    )}
                    {(searchedSongs.length > 0) && (
                    <SearchMainSongs songs={searchedSongs}/>)}
                </div>
                <SearchGroup>
                    {(searchedArtists.length > 0) && (
                        <>
                            <h3 className="library-small-titles">Artists</h3>
                            <LibraryArtistsCards artists={searchedArtists}/>
                        </>
                    )}
                </SearchGroup>
                <SearchGroup>
                    {(searchedAlbums.length > 0) && (
                        <>
                            <h3 className="library-small-titles">Albums</h3>
                            <LibraryAlbumsCards albums={searchedAlbums}/>
                        </>
                    )}
                </SearchGroup>
                <SearchGroup>
                    {(searchedPlaylists.length > 0) && (
                        <>
                            <h3 className="library-small-titles">Playlists</h3>
                            <SearchPlaylistsCards playlists={searchedPlaylists}/>
                        </>
                    )}
                </SearchGroup>
            </SearchGroupsWrapper>
            {(inputRef.current.value !== "" && searchedAlbums.length === 0
            && searchedArtists.length === 0 && searchedPlaylists.length === 0
            && searchedSongs.length === 0) && (<p className="no-results">Aucun résultat pour votre recherche</p>)}
       </SearchWrapper>

    )
}
export default Search