import React from 'react';
import styled from 'styled-components';
import '../styles/PlaylistPage/playlistPage.scss';
import '../styles/home.css';
import {useRef, useState, useEffect} from 'react';
import { Link, useLocation } from 'react-router-dom';
import {Song, Album, AlbumCover, Artist} from '../utils/types/type';
import { artistNamesToJSX, convertDuration } from '../utils/functions/globalFunctions';
import { formatColorTableToRGB, getAverageRGB } from '../utils/functions/imageFunctions';
import axios from 'axios';
import {albumCovers} from '../assets/image/album/albumCovers';
import AlbumTracks from '../components/AlbumPage/AlbumTracks';

//Définition des styled-components
type AlbumWrapperProps = {
    dominantColor: string;
}

type AlbumArtistInfoProps = {
    dominantColor: string
}

const AlbumPageWrapper = styled.div<AlbumWrapperProps>`
    display : flex;
    flex-direction: column;
    align-items: center;
    padding: 35px 0 0 0;
    background: linear-gradient(360deg, #161728 65%, ${props => props.dominantColor} 100%);
    background-color : #161728;
    color: #fff;
    position: relative;
    width: 100%;
    overflow-y: scroll;
    overflow-x: hidden;
`

const AlbumArtistInfo = styled.div<AlbumArtistInfoProps>`
    display: flex;
    align-items: center;
    width: fit-content;
    margin :  0px 0 15px 5px;
    height: 40px;
    padding:  0 7px;
    border-radius: 10px;
    background-color : ${props => props.dominantColor};
    cursor: pointer;
    transition: all 0.1s ease-out;

    :hover{
        transform: translate(5px, 0px);
        h3 {
            opacity: 1;
        }
    }

    img{
        height: 25px;
        width: 25px;
        border-radius: 20px;
    }

    h3{
        margin-left: 5px;
        font-size : 16px;
        font-weight: 500;
        opacity: 0.8;
        transition: opacity 0.1s ease-out;
    }
`

const AlbumInfoWrapper = styled.div`
    display: flex;
    margin-left: 60px;
    padding-bottom: 30px;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    width: 100%;
    min-width: 300px;
    min-height: 174px;
    border-radius: 12px;

    
    
`

const AlbumInfoTextWrapper = styled.div`
    display: inherit;
    justify-content: flex-start;
    margin-top: 10px;
    flex-direction: column;
    height: 100%;
    width: 700px;
    padding: 0 15px;
`


function AlbumPage() {

    //Variables et constantes
    const location = useLocation();
    const { albumProp } = location.state;

    //Définition des states
    const [dominantColor, setDominantColor] = useState<string>("");
    const [artists, setArtists] = useState<Artist[]>([]);

    //Définition des fonctions 
    function getLinkToCoverOnDisk({albumProp, albumCovers}: {albumProp: Album, albumCovers: AlbumCover[]}) {
        let coverLink: string = "";
        albumCovers.forEach((albumCover) => {
            if(albumProp.albumId === albumCover.albumId){
                coverLink = albumCover.cover;
                return coverLink;
            }
        })
        return coverLink;
    }

    function loadDominantColorFromPath(path: string) {
        const image = new Image();
        image.src = path;
        image.onload = () => {
            setDominantColor(formatColorTableToRGB(getAverageRGB(image)));
        }  
    }
    loadDominantColorFromPath(getLinkToCoverOnDisk({albumProp, albumCovers}))


    //Définition du useEffect
    useEffect(() => {
        async function fetchAlbumArtists(artistsIds: number[]) {

        let tableArtists: Artist[] = [];
            
            artistsIds.forEach(async (artistId) => {
                await axios.get("http://localhost:8080/artist/" + artistId)
                .then((response: { data: Artist }) =>{
                    tableArtists.push(response.data);
                    if(tableArtists.length === artistsIds.length){
                        setArtists(tableArtists);
                    } 
                }) 
                .catch(() => console.log("Couldn't fetch artists"))  
            })
        }

        fetchAlbumArtists(JSON.parse(albumProp.artistsIds).artistsIds)
    }, [])


    return (
        <AlbumPageWrapper dominantColor={dominantColor}>
            <AlbumInfoWrapper>
                <div className="cover-full">
                    <img src={albumProp.cover} alt={albumProp.alt} title={albumProp.title}/>
                </div>
                <AlbumInfoTextWrapper>
                    <h2 className='playlist-name'>{albumProp.albumTitle}</h2>
                    <Link to='/Artist' className='link' state={{artistProp: artists[0]}}>
                        <AlbumArtistInfo dominantColor={dominantColor}>
                            {artists.length > 0 && <img src={artists[0].picture}/>}
                            <h3>{(artists.length > 0 && artistNamesToJSX(artists))}</h3>
                        </AlbumArtistInfo>
                    </Link>
                    <p className='playlist-description'>Ajouter une description aux albums</p>
                    <p className='playlist-duration'>{"Dureé : " + convertDuration(albumProp.duration)}</p>
                </AlbumInfoTextWrapper>
           </AlbumInfoWrapper>
           <AlbumTracks album={albumProp} songs={albumProp.songs} dominantColor={dominantColor}/>
        </AlbumPageWrapper>
    )

}

export default AlbumPage;