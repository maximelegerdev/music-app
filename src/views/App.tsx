import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Home from './Home';
import Library from './Library';
import Search from './Search';
import WaitingList from './WaitingList';
import Navbar from '../components/Navbar';
import MusicPlayer from '../components/MusicPlayer';
import { createGlobalStyle } from 'styled-components';
import '../styles/style.css';
import Playlist from './Playlist';
import Album from './Album';
import Artist from './Artist'
import { SongContextProvider } from '../utils/context/SongContext';
import { SongSourceContextProvider } from '../utils/context/SongSourceContext';
import { UserContextProvider } from '../utils/context/UserContext';



function App(){

    return(
        <div className = 'layout'>
            <BrowserRouter>
                <UserContextProvider>
                    <SongSourceContextProvider>
                        <SongContextProvider>
                            <MusicPlayer/>
                                <Routes>
                                    <Route path="/" element={<Home/>}/>
                                    <Route path="/Library" element={<Library/>}/>
                                    <Route path="/Search" element={<Search/>}/>
                                    <Route path="/WaitingList" element={<WaitingList/>}/>
                                    <Route path="/Playlist" element = {<Playlist/>}/>
                                    <Route path="/Album" element = {<Album/>}/>
                                    <Route path="/Artist" element={<Artist/>}/>
                                    <Route path="/SongDetails"/>
                                </Routes>
                            <Navbar/>
                        </SongContextProvider>
                    </SongSourceContextProvider>
                </UserContextProvider>
            </BrowserRouter>
        </div>
    )
};

export default App
