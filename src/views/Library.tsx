import React, { useEffect, useState } from 'react'
import styled from 'styled-components';
import axios from 'axios';
import songs from '../data/songs';
import { setDefaultResultOrder } from 'dns';
import Track from '../components/Track';
import '../styles/library.scss';
import { table } from 'console';
import test from '../assets/artists/anbuu.jpg'
import artists from '../data/artists';
import LibraryArtistsCards from '../components/LibraryAndSearch/LibraryArtistsCards';
import LibraryAlbumsCards from '../components/LibraryAndSearch/LibraryAlbumsCards';
import LibraryPlaylistsCards from '../components/LibraryAndSearch/LibraryPlaylistsCards';
import { User, Playlist, Artist, Album } from '../utils/types/type';

//Défintion des styled components
const LibraryWrapper = styled.div`
    display : flex;
    flex-direction: column;
    padding: 20px 30px;
    padding-right: 0;
    background-color : #161728;
    color: #fff;
    position: relative;
    width: 100%;
    overflow-y: scroll;
    overflow-x: hidden;
`

const FavoritesWrapper = styled.div`
    margin-bottom: 5px;
`

function Library() {
    
    //Définition des states
    const [user, setUser] = useState({
            username: "",
        });

    const [likedPlaylists, setLikedPlaylists] = useState<Playlist[]>([]);
    const [likedArtists, setLikedArtists] = useState<Artist[]>([]);
    const [likedAlbums, setLikedAlbums] = useState<Album[]>([]);

    //states liés au fetch des données
    const [playlistsFetch, setPlaylistsFetch] = useState(false);
    const [artistsFetch, setArtistsFetch] = useState(false);
    const [albumsFetch, setAlbumsFetch] = useState(false);

    //Définition des variables

    //Définition des fonctions
    async function fetchArtistsForLibrary(artistIds: number[]) {
        
        let tableArtists : Artist[] = [];

        artistIds.forEach( async (artistId) => {
            await axios.get("http://localhost:8080/artist/" + artistId)
            .then((response) => {
                tableArtists.push(response.data);
                setLikedArtists(tableArtists);
                (tableArtists.length === artistIds.length) ? setArtistsFetch(true) : setArtistsFetch(false);
                
            })
            .catch(() => console.log("Couldn't fetch artist"))
        });
    }

    async function fetchAlbumsForLibrary(albumIds: number[]){

        let tableAlbums : Album[] = [];

        albumIds.forEach(async (albumId) => {
            await axios.get("http://localhost:8080/album/" + albumId)
            .then((response) => {
                tableAlbums.push(response.data);
                setLikedAlbums(tableAlbums);
                (tableAlbums.length === albumIds.length) ? setAlbumsFetch(true) : setAlbumsFetch(false);
                console.log(tableAlbums);
            })
            .catch(() => console.log("Couldn't fetch albums"))
        });
    }

    async function fetchPlaylistsForLibrary(playlistsIds: number[]) {

        let tablePlaylists : Playlist[] = [];

        playlistsIds.forEach(async (playlistId) => {
            await axios.get("http://localhost:8080/playlist/" + playlistId)
            .then((response) => {
                tablePlaylists.push(response.data);
                setLikedPlaylists(tablePlaylists);
                (tablePlaylists.length === playlistsIds.length) ? setPlaylistsFetch(true) : setPlaylistsFetch(false);
            })
            .catch(() => console.log("Couldn't fetch playlist"))
        })
    }

    //UseEffect pour fetch les données (moins bonne solution du fait de l'existence de ReactQuery)
    useEffect(() => {
        async function fetchLibrary() {
            await axios.get("http://localhost:8080/user/1")
            .then((response) => {
                setUser(response.data);
                fetchArtistsForLibrary(JSON.parse(response.data.likedArtists).likedArtistsIds);
                fetchPlaylistsForLibrary(JSON.parse(response.data.likedPlaylists).likedPlaylistsIds)
                fetchAlbumsForLibrary(JSON.parse(response.data.likedAlbums).likedAlbumsIds)
            })
            .catch(() => console.log("Couldn't fetch user"))
        } 
        fetchLibrary();
    }, [])
    return (
        
        <LibraryWrapper>
            <h1 className="library-title">Library</h1>
            <FavoritesWrapper>
                <h3 className="library-small-titles">Favorite artists</h3>
                {(artistsFetch) && (
                    <LibraryArtistsCards artists={likedArtists}/>
                )}
            </FavoritesWrapper>
            <FavoritesWrapper>
                <h3 className="library-small-titles">Favorite playlists</h3>
                {playlistsFetch && (
                    <LibraryPlaylistsCards playlists={likedPlaylists} username={user.username}/>
                )}
            </FavoritesWrapper>
            <FavoritesWrapper>
                <h3 className="library-small-titles">Favorite albums</h3>
                {albumsFetch && (
                    <LibraryAlbumsCards albums={likedAlbums}/>
                )}
            </FavoritesWrapper>
        </LibraryWrapper>
    )
}
export default Library