import React from 'react';
import styled from 'styled-components';
import '../styles/PlaylistPage/playlistPage.scss';
import '../styles/home.css'
import {useRef, useState, useEffect} from 'react';
import { useLocation } from 'react-router-dom';
import {Song, Playlist, PlaylistCover} from '../utils/types/type'
import { convertDuration } from '../utils/functions/globalFunctions'
import {formatColorTableToRGB, getAverageRGB } from '../utils/functions/imageFunctions';
import axios from 'axios';
import { playlistCovers } from '../assets/image/playlist/playlistCovers';
import PlaylistTracks from '../components/PlaylistPage/PlaylistTracks';

//Définition des styled-components
interface PlaylistWrapperProps {
    dominantColor: string;
}

type PlaylistUserInfoProps = {
    dominantColor: string;
}

const PlaylistWrapper = styled.div<PlaylistWrapperProps>`
    display : flex;
    flex-direction: column;
    align-items: center;
    padding: 35px 0 0 0;
    background: linear-gradient(360deg, #161728 65%, ${props => props.dominantColor} 100%);
    background-color : #161728;
    color: #fff;
    position: relative;
    width: 100%;
    overflow-y: scroll;
    overflow-x: hidden;
`
const PlaylistTracksWrapper = styled.div`
    display: flex;
    flex-direction: column;
`

const PlaylistInfoWrapper = styled.div`
    display: flex;
    margin-left: 60px;
    padding-bottom: 30px;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
    width: 100%;
    min-width: 300px;
    min-height: 174px;
    border-radius: 12px;
`

const PlaylistInfoTextWrapper = styled.div`
    display: inherit;
    justify-content: flex-start;
    margin-top: 10px;
    flex-direction: column;
    height: 100%;
    width: 700px;
    padding: 0 15px;
`

const PlaylistUserInfo = styled.div<PlaylistUserInfoProps>`
    display: flex;
    align-items: center;
    width: fit-content;
    margin :  0px 0 15px 5px;
    height: 40px;
    padding:  0 7px;
    border-radius: 10px;
    background-color : ${props => props.dominantColor};
    cursor: pointer;
    transition: all 0.1s ease-out;

    :hover{
        transform: translate(5px, 0px);
        h3 {
            opacity: 1;
        }
    }

    img{
        height: 25px;
        width: 25px;
        border-radius: 20px;
    }

    h3{
        margin-left: 5px;
        font-size : 16px;
        font-weight: 500;
        opacity: 0.8;
        transition: opacity 0.1s ease-out;
    }
`

type PlaylistPageProps = {
    playlistId : number
}

function PlaylistPage () {

    //Variables et constantes
    const location = useLocation();
    const { playlistProp } = location.state;

    //Définition des refs
    const coverRef = useRef(document.createElement("img"));

    //Définition des states 
    const [dominantColor, setDominantColor] = useState<string>("");
    const [playlistSongs, setPlaylistSongs] = useState<Song[]>([]);
    const [songsFetch, setSongsFetch] = useState(false);

    //Définition des fonctions
    function getLinkToCoverOnDisk({playlistProp, playlistCovers} : {playlistProp: Playlist, playlistCovers: PlaylistCover[]}) {
        let coverLink : string = "";
        playlistCovers.forEach((playlistCover) => {
            if(playlistProp.playlistId === playlistCover.playlistId){
                coverLink = playlistCover.cover;
                return coverLink;
            }
        })
        return coverLink;
    }

    function loadDominantColorFromPath(path: string) {
        const image = new Image();
        image.src = path;
        image.onload = () => {
            setDominantColor(formatColorTableToRGB(getAverageRGB(image)));
        }  
    }

    loadDominantColorFromPath(getLinkToCoverOnDisk({playlistProp, playlistCovers}))

    //Définition du useEffect 
    useEffect(() => {
        async function fetchPlaylistSongs(songsIds: number[]) {

            let tableSongs : Song[] = [];
    
            songsIds.forEach(async (songId) => {
                await axios.get("http://localhost:8080/song/" + songId)
                .then((response) => {
                    tableSongs.push(response.data);
                    setPlaylistSongs(tableSongs);
                    (tableSongs.length === songsIds.length) ? setSongsFetch(true) : setSongsFetch(false);
                })
                .catch(() => console.log("Couldn't fetch playlist songs"))
            })
        }
        fetchPlaylistSongs(JSON.parse(playlistProp.songsIds).songsIds);
    }, [])

    // loadDominantColorFromPath(getLinkToCoverOnDisk({playlistProp, playlistCovers}));

    return(
        <PlaylistWrapper dominantColor={dominantColor}>
            <PlaylistInfoWrapper>
                
                <div className="cover-full">
                    <img src={playlistProp.cover} ref={coverRef} alt={playlistProp.alt} title={playlistProp.title}/>
                </div>

                {/*Dans le cas d'une cover null*/}
                {/* <div className='cover-grid'>
                    <div className='cover-grid-one'>
                        <img className="covers-for-cover" src={songs[0].cover} ref={coverRef}
                         onLoad={() => loadDominantColorFromPath(coverRef.current.src)}></img>
                    </div>
                    <div className='cover-grid-two'>
                        <img className="covers-for-cover" src={songs[1].cover}></img>
                    </div>
                    <div className='cover-grid-three'>
                        <img className="covers-for-cover" src={songs[2].cover}></img>
                    </div>
                    <div className='cover-grid-four'>
                        <img className="covers-for-cover" src={songs[3].cover}></img>
                    </div>
                </div> */}

                <PlaylistInfoTextWrapper>
                    <h2 className='playlist-name'>{playlistProp.playlistName}</h2>
                    <PlaylistUserInfo dominantColor={dominantColor}>
                        <img src={require('../assets/users/it_s_june.jpg')}/>
                        <h3>{playlistProp.user.username}</h3>
                    </PlaylistUserInfo>
                    <p className='playlist-description'>{playlistProp.description}</p>
                    <p className='playlist-duration'>{"Dureé : " + convertDuration(playlistProp.duration)}</p>
                </PlaylistInfoTextWrapper>
            </PlaylistInfoWrapper>
            {songsFetch && (<PlaylistTracks dominantColor={dominantColor} playlist={playlistProp} songs={playlistSongs}/>)}
        </PlaylistWrapper>
    )

}

export default PlaylistPage