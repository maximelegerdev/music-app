import React, { useContext } from 'react'
import styled from 'styled-components';
import Track from '../components/Track';
import '../styles/home.css'
import songs from '../data/songs'
import PlaylistCard from '../components/PlaylistCard';
import {useRef, useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Playlist, Song } from '../utils/types/type';
import LibraryPlaylistsCards from '../components/LibraryAndSearch/LibraryPlaylistsCards';
import { UserContext } from '../utils/context/UserContext';

const HomeWrapper = styled.div`
    display : flex;
    flex-direction: column;
    padding: 20px 30px;
    padding-right: 0;
    background-color : #161728;
    color: #fff;
    position: relative;
    width: 100%;
    overflow-y: scroll;
    overflow-x: hidden;
`

const TracksAndPlaylistsWrapper = styled.div`
    display: flex;
    flex-direction: column;
`

const TrackWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding-right: 30px;
    min-width: 1035px;
    max-width: 1400px;
`

const PlaylistSectionWrapper = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    overflow-x: auto;
`

// type Song = {
//     id: number, 
//     artist: string, 
//     songTitle: string, 
//     audio: string, 
//     cover: string,
//     alt: string,
//     duration: string
// }

function Home() {

    //Définition des states
    const [playlists, setPlaylists] = useState<Playlist[]>([]);
    const [recentlyPlayedSongs, setRecentlyPlayedSongs] = useState<Song[]>([]);
    const {user} = useContext(UserContext);

    //Définition du UseEffect
    useEffect(() => {
        async function homeFetches() {
            await axios.get(`http://localhost:8080/playlist`)
            .then((response) => {
                setPlaylists(response.data);
            })
            .catch(() => console.log(`Couldn't fetch playlists`));
            
            const tableRecentlyPlayedSongs : Song[] = [];
            const recentlyPlayedIds : number[] = JSON.parse(user!.recentlyPlayed).recentlyPlayedIds;
            recentlyPlayedIds.reverse();

            recentlyPlayedIds.forEach(async (songId : number) => {
                await axios.get("http://localhost:8080/song/" + songId)
                .then((response) => {
                    tableRecentlyPlayedSongs.push(response.data);
                    if (tableRecentlyPlayedSongs.length === recentlyPlayedIds.length){
                        setRecentlyPlayedSongs(tableRecentlyPlayedSongs);
                    }
                })
                .catch(() => console.log("Couldn't fetch recently played song data"));
            });
        }
        homeFetches();
    }, [])

    return (
        <HomeWrapper>
            <h1 className="home-title">Home</h1>
            <TracksAndPlaylistsWrapper>
                <h2 className='track-wrapper-title'>Recently Played</h2>
                {recentlyPlayedSongs.length > 0  && (
                    <TrackWrapper>
                        <Track song = {recentlyPlayedSongs[0]}/>
                        <Track song = {recentlyPlayedSongs[1]}/>
                    </TrackWrapper>
                )}
                {recentlyPlayedSongs.length > 0  && (
                    <TrackWrapper>
                        <Track song = {recentlyPlayedSongs[2]}/>
                        <Track song = {recentlyPlayedSongs[3]}/>
                    </TrackWrapper>
                )}
                <h2 className='track-wrapper-title'>Favorite Playlists</h2>
                {(playlists.length > 0) && (
                    <>
                        <LibraryPlaylistsCards playlists={playlists} username="it's june"/>
                    </>
                )}
            </TracksAndPlaylistsWrapper>
        </HomeWrapper>
    )
}
export default Home