import { useState } from "react";
import { useLocation } from "react-router-dom";
import styled from "styled-components";
import '../styles/ArtistPage/artistPage.scss'
import { faCheck, faPlay } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { artistPictures } from '../assets/image/artist/artistPictures';
import { Artist, ArtistPicture } from "../utils/types/type";
import { formatColorTableToRGB, getAverageRGB } from "../utils/functions/imageFunctions";
import TopTracks from "../components/ArtistPage/TopTracks";

type ArtistWrapperProps = {
    dominantColor: string;
}

const ArtistWrapper = styled.div<ArtistWrapperProps>`
    display : flex;
    flex-direction: column;
    padding: 35px 35px 0 35px;
    background: linear-gradient(360deg, #161728 65%, ${props => props.dominantColor} 100%);
    background-color : #161728;
    color: #fff;
    position: relative;
    width: 100%;
    overflow-y: scroll;
    overflow-x: hidden;
`

const ArtistInfoWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    justify-content: flex-start;
    width: 100%;
    min-height: 150px;
    margin-top: 12px;
    border-radius: 12px;
`

const ArtistInfoTextWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding-top: 12px;
    height: 130px;
    width: 100%;
    min-width: 200px;
    margin-left: 15px;
    p{
        margin: 0;
        font-size: 14px;
        font-weight: 300;
        opacity: 0.8;
    }

    .verified{
        display: flex;
        flex-direction: row;
        width: 150px;
        align-items: center;
        
        p{
            font-size: 12px;
            font-style: normal;
            font-weight: 300;
        }
    }
`

const CheckedIcon = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    background-color : #0066CC;
    height: 18px;
    width: 18px;
    border-radius: 50%;
    
    margin-right: 5px;

    .check-icon{
        height: 10px;
        width: 10px;
        opacity: 1;
    }
`

const ControlsContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 35px;
    width: 150px;
    height: 50px;
`
const MainContent = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    padding: 20px 0;
`

const MostPlayedSongs = styled.div`
    height: 428px;
    width: 60.7%;
`

const ArtistSelection = styled.div`
    height: 428px;
    width: 36%;
    border: 2px solid white;
    border-radius: 15px;
`

function ArtistPage () {

    //Variables et constantes
    const location = useLocation();
    const { artistProp } = location.state;

    //Définition des states
    const [dominantColor, setDominantColor] = useState<string>("");
    const [isFollowed, setIsFollowed] = useState(false);

    //Définition des fonctions
    function getLinkToPictureOnDisk({artistProp, artistPictures} : {artistProp: Artist, artistPictures: ArtistPicture[]}){
        let pictureLink : string = "";
        artistPictures.forEach((artistPicture) => {
            if(artistProp.artistId === artistPicture.artistId){
                pictureLink = artistPicture.picture;
                return pictureLink;
            }
        })
        return pictureLink;
    }

    function loadDominantColorFromPath(path: string) {
        const image = new Image();
        image.src = path;
        image.onload = () => {
            setDominantColor(formatColorTableToRGB(getAverageRGB(image)));
        }  
    }

    loadDominantColorFromPath(getLinkToPictureOnDisk({artistProp, artistPictures}))

    return(
        <ArtistWrapper dominantColor={dominantColor}>
            <ArtistInfoWrapper>
                <div className="artist-cover-full">
                    <img src={artistProp.picture} title={artistProp.pictureAlt} alt={artistProp.pictureAlt}></img>
                </div>
                <ArtistInfoTextWrapper>
                    {(artistProp.isVerified) && <div className="verified">                     
                        <CheckedIcon>
                            <FontAwesomeIcon className="check-icon" icon={faCheck}/>
                        </CheckedIcon>
                        <p>Artiste vérifié</p>   
                    </div>}
                    <h1 className="artist-page-name">{artistProp.name}</h1>
                    <p>{artistProp.monthlyListeners + " monthly listeners"}</p>
                </ArtistInfoTextWrapper>
            </ArtistInfoWrapper>
            <ControlsContainer>
                <div className="artist-play-button">
                    <FontAwesomeIcon className="artist-play-icon" icon={faPlay}/>
                </div>
                <div className="artist-follow-button" onClick={() => setIsFollowed(!isFollowed)}>
                    {isFollowed ? "FOLLOWED" : "FOLLOW"}
                </div>
            </ControlsContainer>
            <MainContent>
                <MostPlayedSongs>
                    <h3 className="artist-section-titles">Popular</h3>
                    <TopTracks dominantColor={dominantColor} songIds={JSON.parse(artistProp.songIds).songsIds}/>
                </MostPlayedSongs>
                <ArtistSelection>
                    <h3 className="artist-section-titles">Selection</h3>
                </ArtistSelection>
            </MainContent>


        </ArtistWrapper>
    )
}

export default ArtistPage;