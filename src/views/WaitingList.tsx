import React from 'react'
import styled from 'styled-components';

const WaitingListWrapper = styled.div`
    display : flex;
    justify-content: center;
    padding 20px 30px;
    align-items: center;
    background-color : #161728;
    color: #fff;
    position: relative;
    width: 100%;
`

function WaitingList() {
    return (
        <WaitingListWrapper>
            Waiting List
        </WaitingListWrapper>
    )
}
export default WaitingList