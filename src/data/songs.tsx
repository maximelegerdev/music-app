type Song = {
    id: number, 
    artist: string, 
    songTitle: string, 
    audio: string, 
    cover: string,
    alt: string,
    duration: string
}

const songs: Song[] = [
    {
        id: 1,
        artist: 'Anbuu, Blue Wednesday',
        songTitle: 'Sixth Station',
        audio: require('../assets/audio/sixth_station.mp3'),
        cover: require('../assets/cover/sixth_station.jpg'),
        alt: "Cover de Sixth Station, par Anbuu et Blue Wednesday",
        duration: "2:41"
    },
    {
        id: 2,
        artist: 'Yasumu',
        songTitle: 'We Met',
        audio: require('../assets/audio/we_met.mp3'),
        cover: require('../assets/cover/we_met.jpg'),
        alt: "Cover de We Met, par Yasumu",
        duration: "2:21"
    },
    {
        id: 3,
        artist: 'Swink, Pandrezz',
        songTitle: 'Coffee Beans',
        audio: require('../assets/audio/coffee_beans.mp3'),
        cover: require('../assets/cover/coffee_beans.jpg'),
        alt: "Cover de Coffee Beans, par Swink et Pandrezz",
        duration: "2:20"
    },

    {
        id: 4,
        artist: 'Nymano, Pandrezz',
        songTitle: 'Back Home',
        audio: require('../assets/audio/back_home.mp3'),
        cover: require('../assets/cover/back_home.jpg'),
        alt: "Cover de Back Home, par Nymano et Pandrezz",
        duration: "2:39"
    },

    {
        id: 5,
        artist: 'Idealism',
        songTitle: 'Controlla',
        audio: require('../assets/audio/controlla.mp3'),
        cover: require('../assets/cover/controlla.jpg'),
        alt: "Cover de Controlla, par Idealism",
        duration: "1:48"
    },

    {
        id: 6,
        artist: 'Nothingtosay',
        songTitle: 'Inspect',
        audio: require('../assets/audio/inspect.mp3'),
        cover: require('../assets/cover/inspect.jpg'),
        alt: "Cover de Inspect, par nothingtosay",
        duration: "2:58" 
    },

    {
        id: 7,
        artist: 'Saib',
        songTitle: 'Sakura Trees',
        audio: require('../assets/audio/sakura_trees.mp3'),
        cover: require('../assets/cover/sakura_trees.jpg'),
        alt: "Cover de Sakura Trees, par Saib",
        duration: "1:40" 
    },

    {
        id: 8,
        artist: 'Kupla',
        songTitle: 'Valentine',
        audio: require('../assets/audio/valentine.mp3'),
        cover: require('../assets/cover/valentine.jpg'),
        alt: "Cover de Valentine, par Kupla",
        duration: "3:51"
    },

    {
        id: 9,
        artist: 'Swink',
        songTitle: 'Loneliness',
        audio: require('../assets/audio/loneliness.mp3'),
        cover: require('../assets/cover/hibernation.jpg'),
        alt: "Cover de Loneliness, par Swink",
        duration: "2:11"
    },

    {
        id: 10,
        artist: 'Swink',
        songTitle: 'One Way Ticket',
        audio: require('../assets/audio/one_way_ticket.mp3'),
        cover: require('../assets/cover/hibernation.jpg'),
        alt: "Cover de One Way Ticket, par Swink",
        duration: "2:35"
    },
    
    {
        id: 11,
        artist: 'Swink',
        songTitle: 'Childhood',
        audio: require('../assets/audio/childhood.mp3'),
        cover: require('../assets/cover/hibernation.jpg'),
        alt: "Cover de Childhood, par Swink",
        duration: "2:36"
    },

    {
        id: 12,
        artist: 'Swink',
        songTitle: 'Cocoon',
        audio: require('../assets/audio/cocoon.mp3'),
        cover: require('../assets/cover/hibernation.jpg'),
        alt: "Cover de Cocoon, par Swink",
        duration: "2:50"
    },

    {
        id: 13,
        artist: 'Swink',
        songTitle: 'Glow',
        audio: require('../assets/audio/glow.mp3'),
        cover: require('../assets/cover/hibernation.jpg'),
        alt: "Cover de Glow, par Swink",
        duration: "2:55"
    },

    {
        id: 14,
        artist: 'Swink',
        songTitle: 'Ghost',
        audio: require('../assets/audio/ghost.mp3'),
        cover: require('../assets/cover/hibernation.jpg'),
        alt: "Cover de Ghost, par Swink",
        duration: "2:55"
    }
]

export default songs
