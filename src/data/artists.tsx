type Artists = {
    id: number,
    name: string,
    songsId: number[],
}

const artists: Artists[] = [
    {
        id: 1,
        name : "Anbuu",
        songsId: [1],
    },
    {
        id: 2,
        name : "Blue Wednesday",
        songsId: [1],
    },
    {
        id: 3,
        name : "Yasumu",
        songsId: [2],
    },
    {
        id: 4,
        name : "Swink",
        songsId: [3],
    },
    {
        id: 5,
        name : "Pandrezz",
        songsId: [3, 4],
    },
    {
        id: 6,
        name : "Nymano",
        songsId: [4],
    },
    {
        id: 7,
        name : "Idealism",
        songsId: [5],
    },
    {
        id: 8,
        name : "Nothingtosay",
        songsId: [6],
    },
    {
        id: 9,
        name : "Saib",
        songsId: [7],
    },
    {
        id: 10,
        name : "Kupla",
        songsId: [8],
    },
]

export default artists