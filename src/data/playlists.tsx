
type Playlist = {
    id: number, 
    name: string, 
    idUser: number, 
    idsSongs: number[], 
    nbrSongs: number,
    cover: string,
    duration: string,
    creationDate: string,
    description: string,
    isFavorite: boolean
}

const playlists: Playlist[] = [
    {
        id: 1,
        name: "Playlist sympa",
        idUser: 1,
        idsSongs: [1, 2, 7, 4],
        nbrSongs: 4,
        cover: "",
        duration: "",
        creationDate: "12/10/2022",
        description: "Playlist une sympa tu connais, la première de ce document",
        isFavorite: true
    },
    {
        id: 2,
        name: "Deux",
        idUser: 1,
        idsSongs: [3, 4, 6, 7],
        nbrSongs: 4,
        cover: "",
        duration: "",
        creationDate: "12/10/2022",
        description: "Playlist deux c'est cool aussi voilà",
        isFavorite: false
    },
    {
        id: 3,
        name: "Trois",
        idUser: 1,
        idsSongs: [1, 2, 4, 6, 7],
        nbrSongs: 5,
        cover: "",
        duration: "",
        creationDate: "12/10/2022",
        description: "Playlist trois parce que jamais deux sans trois",
        isFavorite: false
    },
    {
        id: 4,
        name: "Quatre",
        idUser: 1,
        idsSongs: [2, 3, 4, 5, 6],
        nbrSongs: 5,
        cover: "",
        duration: "",
        creationDate: "12/10/2022",
        description: "La quatrième lessgo ma leugueu",
        isFavorite: true
    }
]

export default playlists