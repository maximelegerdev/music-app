import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import App from './views/App';
import Library from './views/Library';
import Search from './views/Search';
import WaitingList from './views/WaitingList';
import MusicPlayer from './components/MusicPlayer';
import { createGlobalStyle } from 'styled-components';
import './styles/style.css';


ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
)

