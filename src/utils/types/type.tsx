export type Artist = {
    albumIds: string,
    artistId: number,
    followers: number,
    name: string,
    picture: string,
    pictureAlt: string,
    songIds: string,
    style: string,
    monthlyListeners: number;
    isVerified: boolean;
}

export type ArtistPicture = {
    artistId: number;
    picture: string;
}

export type Playlist = {
    playlistId: number,
    cover: string,
    coverAlt: string,
    creationDate: Date,
    duration: string,
    lastListeningDate: Date,
    likes: number,
    playlistName: string,
    songsIds: string,
    songsNumber: number,
    style: string
    user: User,
    description: string
}

export type PlaylistCover = {
    playlistId : number;
    cover : string
}

export type Album = {
    albumId: number,
    albumTitle: string,
    albumType: string,
    cover: string,
    coverAlt: string,
    duration: string,
    likes: number,
    releaseDate : Date,
    style: string,
    artistsIds: string,
    songsNumber : number,
    songs: Song[]
}

export type AlbumCover = {
    albumId: number;
    cover: string;
}

export type Song = {
    songId: number,
    songTitle: string,
    artistsIds: string,
    duration: string,
    album: Album | null,
    releaseDate: Date,
    song: string,
    cover: string,
    likes: number,
    coverAlt: string,
    style: string,
    streams: number,
    isExplicit: boolean
}

export type User = {
    userId: number,
    likedAlbums: string,
    likedArtists: string,
    likedPlaylists: string, 
    likedSongs: string,
    password: string,
    picture: string,
    pictureAlt: string,
    recentlyPlayed: string,
    userStatus: number,
    username: string
}