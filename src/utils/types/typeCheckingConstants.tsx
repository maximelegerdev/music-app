import {Album, Playlist} from './type'

// export const checkAlbum : Album = {
//     "albumId": 1,
//     "albumTitle": "Hibernation",
//     "artistsIds": "{\"artistsIds\" : [4]}",
//     "releaseDate": new Date(1661292000000),
//     "cover": "https://i.scdn.co/image/ab67616d00001e027a550271976042d2b393d369",
//     "albumType": "EP",
//     "coverAlt": "Cover de Hibernation, par Swink",
//     "duration": "00:16:04",
//     "likes": 1,
//     "style": "Lofi Hip-Hop",
//     "songsNumber": 6
// };

export const checkPlaylist : Playlist = {"playlistId": 1,
    "playlistName": "Breathe",
    "songsIds": "{ \"songsIds\": [1, 3, 4]}",
    "songsNumber": 3,
    "duration": "00:07:40",
    "user": {
        "userId": 1,
        "username": "it's june",
        "password": "Password_mocktify",
        "picture": "require('../assets/users/it_s_june.jpg')",
        "likedPlaylists": "{\"likedPlaylistsIds\" : [1, 2, 3]}",
        "likedAlbums": "{ \"likedAlbumsIds\" : [1] }",
        "likedArtists": "{ \"likedArtistsIds\" : [1, 3, 4] }",
        "likedSongs": "{ \"likedSongsIds\" : [1, 3, 4] }",
        "recentlyPlayed": "{ \"recentlyPlayedIds\" : [1, 3, 4] }",
        "userStatus": 2,
        "pictureAlt": "Photo d'it's june"
    },
    "cover": "https://i.scdn.co/image/ab67616d0000b273be1540bc929d68e5684e949f",
    "creationDate": new Date(1667343600000),
    "lastListeningDate": new Date(1667343600000),
    "likes": 1,
    "coverAlt": "Cover de la playlist Breathe",
    "style": "Lofi Hip-Hop",
    "description": "test"
};