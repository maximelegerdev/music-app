import songs from "../../data/songs";
import { Playlist, Song } from "../types/type";
import { Artist } from "../types/type"

//Permet de retourner une chaine contenant le nom des artistes depuis un tableau de string les contenant
export function artistNames(artists: String[]){
    let artistsConcat : String = "";
    if(artists.length > 1) {
        artists.forEach((artist) => {
            artistsConcat = (artistsConcat === "") ? artist : (artistsConcat + ", " + artist);
        })
    } else {
        artistsConcat = artists[0];
    }
    return artistsConcat;
}

export function artistNamesToJSX(artists: Artist[]){
    let artistsConcat : JSX.Element[] = [];
    if(artists.length > 1) {
        artists.forEach((artist) => {
            artistsConcat = (artistsConcat.length === 0) ? [<a>{artist.name}</a>] : artistsConcat.concat([<a className="comma">,&nbsp;</a>, <a>{artist.name}</a>]);
        })
    } else {
        artistsConcat = [<a>{artists[0].name}</a>];
    }
    return artistsConcat;
}

//Permet de passer d'un nombre de secondes à un format string de minutes et secondes au format MM:SS
export function timeFormat(time: number) {
    let minutes: number = Math.floor(time/60);
    let seconds: number = Math.floor(time % 60);
    let secondsString: string = seconds.toString();

    if (seconds < 10) {
        secondsString = "0"+seconds;
    }

    return minutes + ':' + secondsString;
}

//Permet de convertir un String hh:mm:ss en string 00h 00min 00s
export function convertDuration(duration: string) : string {
    let conversion: string = "";
    if(duration.substring(0, 2) === "00"){
        conversion = duration.substring(3, 5) + " min " 
                + duration.substring(6, 8) + " s ";
    } else {
        conversion = duration.substring(0, 2) + " h " 
                + duration.substring(3, 5) + " min "
    }
    return conversion;
}

//Permet de faire passer la playlist Liked Songs avant toutes les autres dans la liste des playlists si elle y est
export function trierPlaylist(playlists: Playlist[]) : Playlist[] {
    let exchange: Playlist = playlists[0];
    let table: Playlist[] = [];

    playlists.forEach((playlist) => {
        if(playlist.playlistName === "Liked songs"){
            exchange = playlist;
        }
    })

    table.push(exchange);
    playlists.forEach((playlist) => {
        if(playlist.playlistName !== "Liked songs"){
            table.push(playlist);
        }
    }) 

    return table;
}

//Permet de trier les sons en fonction de leur songId
export function sortSongsById(songs: Song[]) {
    songs.sort((a, b) => {
        if (a.songId < b.songId) {
            return -1;
        } else if (a.songId > b.songId) {
            return 1;
        } else {
            return 0;
        }
    });
    return songs;
}

//Permet de trier les sons en fonction de leurs streams
export function sortSongsByStreams(songs: Song[]) {
    songs.sort((a, b) => {
        if (a.streams > b.streams) {
            return -1;
        } else if (a.streams < b.streams) {
            return 1;
        } else {
            return 0;
        }
    });
    return songs;
}

//Permet de retourner le lien de l'audio, du nom des artistes
export function getAudioAndArtistNamesForSong(contextSong: Song){
    const audioAndArtist : {audio: string, artist: string} = {audio: '',  artist:''};
    songs.forEach((song) => {
        (contextSong.songId === song.id) && (audioAndArtist.audio = song.audio) 
        && (audioAndArtist.artist = song.artist);
    })

    return audioAndArtist;
}

//Permet de slice les tops sons
export function sliceTopSongs(table: Song[]) {
    if(table.length > 5) {
        return table.slice(0, 5);
    } else {
        return table.slice(0, table.length);
    }
}

  
  