//Récupère la couleur moyenne d'une image
export function getAverageRGB(imgEl: HTMLImageElement) : number[] {

  let tableRGB: number[] = [];

  let blockSize = 5, // only visit every 5 pixels
      defaultRGB = {r:0,g:0,b:0}, // for non-supporting envs
      canvas = document.createElement('canvas'),
      context = canvas.getContext && canvas.getContext('2d'),
      data, width, height,
      i = -4,
      length,
      rgb = {r:0,g:0,b:0},
      count = 0;

  if (!context) {
      tableRGB = [defaultRGB.r, defaultRGB.g, defaultRGB.b];
      return tableRGB;
  }

  height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
  width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;

  context.drawImage(imgEl, 0, 0);

  try {
      data = context.getImageData(0, 0, width, height);
  } catch(e) {
      /* security error, img on diff domain */
      tableRGB = [defaultRGB.r, defaultRGB.g, defaultRGB.b];
      return tableRGB;
  }

  length = data.data.length;

  while ( (i += blockSize * 4) < length ) {
      ++count;
      rgb.r += data.data[i];
      rgb.g += data.data[i+1];
      rgb.b += data.data[i+2];
  }

  // ~~ used to floor values
  rgb.r = ~~(rgb.r/count);
  rgb.g = ~~(rgb.g/count);
  rgb.b = ~~(rgb.b/count);

  tableRGB = [rgb.r, rgb.g, rgb.b];

  return tableRGB;

}
  

//Récupérer la couleur dominante d'une image
export function getDominantColor(image: HTMLImageElement): [number, number, number] {
    // Création d'un objet canvas
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
  
    if (!context) {
        throw new Error('Le contexte de dessin du canvas n\'a pas pu être créé');
    }

    // Redimensionnement du canvas à la taille de l'image
    canvas.width = image.width;
    canvas.height = image.height;
  
    // Dessin de l'image dans le canvas
    context.drawImage(image, 0, 0);
  
    // Récupération des données des pixels de l'image
    const imageData = context.getImageData(0, 0, canvas.width, canvas.height);
    const pixels = imageData.data;
  
    // Tableau pour compter le nombre de pixels de chaque couleur
    const colorCounts: { [key: string]: number } = {};
    let dominantColor: [number, number, number] | null = null;
    let dominantColorCount = 0;
  
    // Parcours des pixels de l'image
    for (let i = 0; i < pixels.length; i += 4) {
      // Récupération de la couleur du pixel sous forme de triplet RGB
      const color: [number, number, number] = [pixels[i], pixels[i+1], pixels[i+2]];

      // Conversion du triplet RGB en chaîne de caractères
      const colorString = `rgb(${color[0]}, ${color[1]}, ${color[2]})`;

      // Si la couleur n'est pas encore présente dans le tableau, on l'ajoute avec un compteur à 1
      if (!colorCounts[colorString]) {
        colorCounts[colorString] = 1;
      }
      // Sinon, on incrémente le compteur de cette couleur
      else {
        colorCounts[colorString]++;
      }
  
      // Si le compteur de cette couleur est supérieur au compteur de la couleur dominante actuelle,
      // on met à jour la couleur dominante et son compteur
      if (colorCounts[colorString] > dominantColorCount) {
        dominantColor = color;
        dominantColorCount = colorCounts[colorString];
      }
    }
  
    return dominantColor!; // La couleur dominante ne peut pas être nulle
}

export function formatColorTableToRGB(table: number[]) {
    const color = `rgb(${table[0]}, ${table[1]}, ${table[2]}, 0.4)`;
    return color;
}