import React, {createContext, useState} from 'react';
import {Song} from '../types/type'

type SongContextType = {
    contextSong : Song | null;
    setContextSong : React.Dispatch<React.SetStateAction<Song>>
    songState : boolean;
    setSongState : React.Dispatch<React.SetStateAction<boolean>>
}

type SongContextProviderProps = {
    children: React.ReactNode;
}

const defaultSong : Song = {
    "songId": 1,
    "songTitle": "Sixth Station",
    "artistsIds": "{ \"artistsIds\" : [1,2]}",
    "duration": "00:02:41",
    "album": null,
    "releaseDate": new Date(1610751600000),
    "song": "../assets/audio/sixth_station.mp3",
    "cover": "https://i.scdn.co/image/ab67616d0000b273e5347ad1af00f6f599dd4bf9",
    "likes": 0,
    "coverAlt": "Cover de Sixth Station, par Anbuu et Blue Wednesday",
    "style": "Lofi Hip-Hop",
    "streams": 12345678,
    "isExplicit": false,
}

export const SongContext = React.createContext({} as SongContextType);

export const SongContextProvider = ({children} : SongContextProviderProps) => {

    const [contextSong, setContextSong] = useState<Song>(defaultSong);
    const [songState, setSongState] = useState(false);

    return(
        <SongContext.Provider value={{contextSong, setContextSong, songState, setSongState}}>
            {children}
        </SongContext.Provider>
    )
}