import React, {createContext, useState } from 'react';
import { User } from '../types/type';


const defaultUser = {
    "userId": 1,
    "username": "it's june",
    "password": "Password_mocktify",
    "picture": "require('../assets/users/it_s_june.jpg')",
    "likedPlaylists": "{\"likedPlaylistsIds\" : [1, 2, 3]}",
    "likedAlbums": "{ \"likedAlbumsIds\" : [1] }",
    "likedArtists": "{ \"likedArtistsIds\" : [1, 3, 4] }",
    "likedSongs": "{ \"likedSongsIds\" : [1, 3, 4] }",
    "recentlyPlayed": "{ \"recentlyPlayedIds\" : [1, 3, 4, 6] }",
    "userStatus": 2,
    "pictureAlt": "Photo d'it's june"
}

type UserContextType = {
    user: User | null,
    setUser : React.Dispatch<React.SetStateAction<User | null>>
}

type UserContextProviderProps = {
    children: React.ReactNode
}

export const UserContext = React.createContext({} as UserContextType);

export const UserContextProvider = ({children} : UserContextProviderProps) => {
    const [user, setUser] = useState<User | null>(defaultUser);
    return (
        <UserContext.Provider value={{user, setUser}}>
            {children}
        </UserContext.Provider>
    )
}