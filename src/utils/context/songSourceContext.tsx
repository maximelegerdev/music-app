import axios from 'axios';
import React, {createContext, useContext, useEffect, useState } from 'react';
import { sortSongsById } from '../functions/globalFunctions';
import { Playlist, Album, Song } from '../types/type';

type SongSourceContextType = {
    songSource : Album | Playlist
    setSongSource : React.Dispatch<React.SetStateAction<Album | Playlist>>
}

type SongSourceContextProviderProps = {
    children: React.ReactNode
}

const defaultSongSource : Playlist = {
    "playlistId": 0,
    "playlistName": "Catalog",
    "songsIds": "{\"songsIds\": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}",
    "songsNumber": 14,
    "duration": "00:54:21",
    "user": {
        "userId": 1,
        "username": "it's june",
        "password": "Password_mocktify",
        "picture": "require('../assets/users/it_s_june.jpg')",
        "likedPlaylists": "{\"likedPlaylistsIds\" : [1, 2, 3]}",
        "likedAlbums": "{ \"likedAlbumsIds\" : [1] }",
        "likedArtists": "{ \"likedArtistsIds\" : [1, 3, 4] }",
        "likedSongs": "{ \"likedSongsIds\" : [1, 3, 4] }",
        "recentlyPlayed": "{ \"recentlyPlayedIds\" : [1, 3, 4] }",
        "userStatus": 2,
        "pictureAlt": "Photo d'it's june"
    },
    "cover": require("../../assets/image/logo-mock.png"),
    "creationDate": new Date(1667948400000),
    "lastListeningDate": new Date(1667948400000),
    "likes": 0,
    "coverAlt": "Cover du Catalogue",
    "style": "Tous les styles",
    "description": "Ensemble du catalogue Mocktify"
}

const defaultPlaylist : Playlist = {
    "playlistId": 3,
    "playlistName": "Liked songs",
    "songsIds": "{\"songsIds\": [1, 3, 4]}",
    "songsNumber": 3,
    "duration": "00:07:40",
    "user": {
        "userId": 1,
        "username": "it's june",
        "password": "Password_mocktify",
        "picture": "require('../assets/users/it_s_june.jpg')",
        "likedPlaylists": "{\"likedPlaylistsIds\" : [1, 2, 3]}",
        "likedAlbums": "{ \"likedAlbumsIds\" : [1] }",
        "likedArtists": "{ \"likedArtistsIds\" : [1, 3, 4] }",
        "likedSongs": "{ \"likedSongsIds\" : [1, 3, 4] }",
        "recentlyPlayed": "{ \"recentlyPlayedIds\" : [1, 3, 4] }",
        "userStatus": 2,
        "pictureAlt": "Photo d'it's june"
    },
    "cover": "https://i1.sndcdn.com/artworks-y6qitUuZoS6y8LQo-5s2pPA-t500x500.jpg",
    "creationDate": new Date(1667948400000),
    "lastListeningDate": new Date(1667948400000),
    "likes": 1,
    "coverAlt": "Cover des titres favoris",
    "style": "Lofi Hip-Hop",
    "description": "test"
};

const defaultAlbum : Album = {
    "albumId": 1,
    "albumTitle": "Hibernation",
    "artistsIds": "{\"artistsIds\" : [4]}",
    "releaseDate": new Date(1661292000000),
    "cover": "https://i.scdn.co/image/ab67616d00001e027a550271976042d2b393d369",
    "albumType": "EP",
    "coverAlt": "Cover de Hibernation, par Swink",
    "duration": "00:16:04",
    "likes": 1,
    "style": "Lofi Hip-Hop",
    "songsNumber": 6,
    "songs": []
};

export const SongSourceContext = React.createContext({}  as SongSourceContextType);

export const SongSourceContextProvider = ({children} : SongSourceContextProviderProps) => {
     
    const [songSource, setSongSource] = useState<Album | Playlist>(defaultSongSource);

    return(
        <SongSourceContext.Provider value={{songSource, setSongSource}}>
            {children}
        </SongSourceContext.Provider>
    )
}
